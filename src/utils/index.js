import {
  catalogueData,
  compainesList,
  vacancies,
  media,
  inbox,
} from './dummyData';

import {validateField,validateFields} from './Validations';
import {
  LoginFormState,
  ForgetPasswordFormState,
  SignupFormState,
  PasswordFormState
} from './formInitialState';


import {fbHandler,googleSigning} from './helper'

export {
  LoginFormState,
  ForgetPasswordFormState,
  PasswordFormState,
  SignupFormState,
  validateFields,
  validateField,
  catalogueData,
  compainesList,
  vacancies,
  media,
  inbox,

};

export {
  fbHandler,googleSigning
}