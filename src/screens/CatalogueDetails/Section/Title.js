import React from 'react';
import {Text, Block} from 'components';
import {colors, sizes} from 'styles/theme';

const Title = () => {
  const data = [
    {key: '1', name: 'Business'},
    {key: '2', name: 'Finance'},
    {key: '3', name: 'Manufacturing'},
  ];
  return (
    <Block flex={1}>
      <Text h2 bold>
        The Business Intelligence Analyst Course 2019
      </Text>
      <Text h4 color={colors.gray}>
        Created By Danuel Wallingston, Instructor HQ
      </Text>
      <Block row>
        {data.map(el => {
          return (
            <Block
            margin={[sizes.getHeight(1),sizes.getWidth(2),0,0]}
            padding={[sizes.getHeight(2),sizes.getWidth(2)]}
              center
              middle
              flex={false}
              style={{
                // width: sizes.getWidth(24),
                // borderWidth: 1,
                backgroundColor:'#DBDBDB',
                height: sizes.getHeight(3),
              }}>
              <Text h4>{el.name}</Text>
            </Block>
          );
        })}
      </Block>
    </Block>
  );
};

export default Title;
