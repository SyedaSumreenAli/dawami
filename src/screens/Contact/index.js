import React from 'react';
import {Block, Text, Button, CustomInput, CustomHeader} from 'components';
import {sizes, colors} from 'styles/theme';

import {ScrollView} from 'react-native-gesture-handler';
import ContactInfo from './ContactInfo';
import ContactForm from './ContactForm';

const Contact = ({navigation}) => {
  return (
    <Block>
     <CustomHeader bg={'#AB7264'} navigation={navigation} backBtn />
    <ContactInfo />
    <ContactForm />
    </Block>
  );
};

export default Contact;
