import React from 'react';
import { Text, Block, CustomInput, Button } from 'components'
import {sizes, colors} from 'styles/theme';
import { ScrollView } from 'react-native';

const ContactForm = () => {
  return (
    <Block
      padding={[sizes.getHeight(2), sizes.getWidth(4), 0, sizes.getWidth(4)]}
      style={{
          elevation:10,
        backgroundColor: colors.primary,
        position: 'relative',
        height: sizes.getHeight(60),
        bottom: sizes.getHeight(4),
        width: '88%',
        left: '6%',
      }}>
      <ScrollView style={{height: sizes.getHeight(90)}}>
        <CustomInput noRounded noIcon bb placeholder="Your Name" bc={colors.gray} />
        <CustomInput noRounded noIcon bb placeholder="Email" bc={colors.gray} />
        <CustomInput noRounded noIcon bb placeholder="company" bc={colors.gray} />
        <CustomInput noRounded noIcon bb placeholder="Phone" numeric bc={colors.gray} />
        <CustomInput
          noRounded
          noIcon
          placeholder="Message"
          numberOfLine={30}
          textarea
          height={sizes.getHeight(25)}
        />
      </ScrollView>
      <Button center middle style={{backgroundColor: colors.customRed}}>
        <Text color={colors.primary}> Send</Text>
      </Button>
    </Block>
  );
};

export default ContactForm;
