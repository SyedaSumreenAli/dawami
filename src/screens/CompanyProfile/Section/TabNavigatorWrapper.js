import React, {useState} from 'react';
import {Block, Text, ChatButton} from 'components';
import {TabView, SceneMap, TabBar, ScrollPager} from 'react-native-tab-view';
import {About, Vacancies, Media, Contact} from 'screens/CompanyProfile';
import {colors, sizes} from 'styles/theme';

const TabNavigatorWrapper = ({navigation}) => {
  console.log({navigation})
  const [index, setIndex] = React.useState(0);
  const [routes] = useState([
    {key: 'about', title: 'About'},
    {key: 'vacancies', title: 'Vacancies'},
    {key: 'media', title: 'Media'},
    {key: 'contacts', title: 'Contacts'},
  ]);

  const renderScene = SceneMap({
    about: () => <About />,
    contacts: () => <Contact />,
    media: () => <Media />,
    vacancies: () => <Vacancies />,
  });

  const renderTabBar = props => (
    <TabBar
      {...props}
      indicatorStyle={{backgroundColor: colors.brown}}
      style={{
        backgroundColor: 'transparent',
        elevation: 0,
      }}
      indicatorStyle={{
        borderWidth: 2.2,
        borderColor: colors.brown,
        width: '10%',
        alignSelf: 'center',
        marginLeft: '5%',
        borderTopLeftRadius: sizes.getWidth(10),
        borderTopRightRadius: sizes.getWidth(10),
      }}
      activeColor={colors.brown}
      inactiveColor={colors.black}
      labelStyle={{
        textTransform: 'capitalize',
        fontSize: sizes.withScreen(0.01),
      }}
    />
  );

  return (
    <Block>
      <TabView
        navigationState={{index, routes}}
        renderScene={renderScene}
        onIndexChange={setIndex}
        // initialLayout={()=><Contact />}
        renderTabBar={renderTabBar}
      />
      <ChatButton onPress={()=> navigation.navigate('Messages') } />
    </Block>
  );
};

export default TabNavigatorWrapper;
