import React from 'react';
import {Block} from 'components';
import {media} from 'utils';
import {Image, FlatList} from 'react-native';
import {sizes} from 'styles/theme';

const Media = () => {
  return (
    <Block padding={[0,sizes.getWidth(2)]}>
      <FlatList
        showsHorizontalScrollIndicator={false}
        showsVerticalScrollIndicator={false}
        data={media}
    numColumns={3}
        renderItem={({item}) => {
          return (
            <Block
                margin={[5,5]}
                center middle
              height={sizes.getHeight(13)}
              width={sizes.getWidth(33)}
              style={{
                borderRadius:sizes.screenSize * 0.011,
                overflow: 'hidden',
              }}>
              <Image
                source={item.image}
                style={{resizeMode: 'center', flex:0.9}}
              />
            </Block>
          );
        }}
      />

    </Block>
  );
};

export default Media;
