import React from 'react';
import {Block, Text, Button, ChatButton} from 'components';
import {vacancies} from 'utils';
import {sizes, colors} from 'styles/theme';
import {Image, FlatList} from 'react-native';
import * as icons from 'assets/icons';
const Vacancies = () => {
  return (
    <Block padding={[20, 0]}>
      <FlatList
        showsHorizontalScrollIndicator={false}
        showsVerticalScrollIndicator={false}
        data={vacancies}
        renderItem={({item}) => {
          return (
            <Block padding={[0, sizes.getWidth(5)]}>
              <Block
                row
                style={{borderBottomWidth: 1, borderBottomColor: colors.gray}}>
                <Block
                  padding={[sizes.getHeight(1), 0, sizes.getHeight(1), 0]}
                  flex={false}
                  width={sizes.getWidth(85)}>
                  <Text h3 bold>
                    {item.title}
                  </Text>
                  <Text h4>{item.subTitle}</Text>
                  <Text h4>{item.data}</Text>
                </Block>
                <Button crossRight middle padding={[10, 0, 0, 0]}>
                  <Image
                    source={icons.empty_heart}
                    style={{
                      resizeMode: 'contain',
                      width: sizes.getWidth(4),
                      tintColor: colors.brown,
                    }}
                  />
                </Button>
              </Block>
            </Block>
          );
        }}
      />
      {/* message */}
    </Block>
  );
};

export default Vacancies;
