import React from 'react';
import {Block, Text} from 'components';
import {Image, ScrollView} from 'react-native';
import * as icons from 'assets/icons';
import * as images from 'assets/images';
import {sizes, colors} from 'styles/theme';

const About = () => {
  return (
    <Block padding={[0, sizes.getWidth(5)]}>
      <Block
        center
        row
        style={{
          borderBottomWidth: 0.5,
          borderBottomColor: colors.gray,
        }}>
        <Image
          source={icons.location}
          style={{
            resizeMode: 'contain',
            marginRight: sizes.getWidth(1),
            width: sizes.getWidth(3),
            marginTop: sizes.getHeight(2),
            tintColor: colors.gray,
          }}
        />
        <Text h3 color={colors.gray} style={{marginTop: sizes.getHeight(2)}}>
          Al Sadd
        </Text>
      </Block>
      <Block margin={[sizes.getHeight(2), 0, 0, 0]} flex={9}>
        <ScrollView style={{flex:1}} showsVerticalScrollIndicator={false}>
          <Text h4 color={colors.gray}>
            Contrary to popular belief, Lorem Ipsum is not simply random text.
            It has roots in a piece of classical Latin literature from 45 BC,
            making it over 2000 years old. Richard McClintock, a Latin professor
            at Hampden-Sydney College in Virginia, looked up one of the more
            obscure Latin words, consectetur, from a Lorem Ipsum passage, and
            going through the cites of the word in classical literature,
            discovered the undoubtable source. Lorem Ipsum comes from sections
            1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes
            of Good and Evil) by Cicero, written in 45 BC. This book is a
            treatise on the theory of ethics, very popular during the
            Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit
            amet..", comes from a line in section 1.10.32. {'\n'} {'\n'} {'\n'}
            Contrary to popular belief, Lorem Ipsum is not simply random text.
            It has roots in a piece of classical Latin literature from 45 BC,
            making it over 2000 years old. Richard McClintock, a Latin professor
            at Hampden-Sydney College in Virginia, looked up one of the more
            obscure Latin words, consectetur, from a Lorem Ipsum passage, and
            going throug...
          </Text>
        </ScrollView>
      </Block>

    </Block>
  );
};

export default About;
