import React from 'react'
import { Block, Text } from 'components'
import { Header,TabWrapper } from 'screens/CompanyProfile'
const CompanyPortfolio = ({navigation}) => {
    return (
       <Block>
           <Header />
           <Block flex={5}>
                <TabWrapper navigation={navigation} />
           </Block>
       </Block>
    )
}

export default CompanyPortfolio
