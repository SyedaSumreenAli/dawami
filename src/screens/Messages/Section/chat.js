import React, {Component} from 'react';
import { ScrollView, View,Text, Image, StyleSheet, TextInput} from 'react-native';
import {height, width, totalSize} from 'react-native-dimension';
// import {Button, Icon} from 'native-base'
import SendMessage from './sendMessage';
import RecieveMessage from './recieveMessage';



class Chat extends Component
{
  render(){
    return(
      <View style={style.body}>

        {/******* HEADER COMPONENT ********/}
        <View style={style.header}>
          <View style={style.arrowContainer}> 
            <Image source={require('assets/icons/goBack_arrow.png')} style={style.goBackArrow}/>
          </View>
          <View style={style.logoContainer}>
            <Image source={require('assets/icons/logo_horizontal.png')} style={style.logo}/>
            <Text style={[style.text, style.h3]}>Qatar National Bank</Text>
          </View>
          <View style={style.langContainer}> 
            <Text style={[style.text, style.h2]} >AR</Text>
          </View>
        </View>

         {/********* CONVERSATION COMPONENT **********/}
         <ScrollView>
           <View style={style.conversationContainer}>
          <SendMessage/>
          <RecieveMessage/>
          <SendMessage/>
          <RecieveMessage/>
          <SendMessage/>
          <RecieveMessage/>
          <SendMessage/>

           </View>
         </ScrollView>

         {/************ INPUT COMPONENT **********/}
          

      </View>
    );
  }
 
};

const style= StyleSheet.create({
  body:{
    backgroundColor:"#fff",
    flex:1
  },
  header:{
    backgroundColor:"#3e5952",
    flexDirection:"row",
    justifyContent:"space-between",
    alignItems:"center",
    height: height(13)
  },
  arrowContainer:{
    alignItems:"center",
    flex:1,
    // backgroundColor:"red",
  
  },
  logoContainer:{
    alignItems:"center",
    flex:5,
    // backgroundColor:"skyblue",
  
  },
  langContainer:{
    alignItems:"center",
    flex:1,
    paddingBottom:height(3)
    // backgroundColor:"pink",
  
  },
  goBackArrow:{   
    height:height(3),
    marginBottom:height(3)
  },
  logo:{
    height:height(8.1),
    width:width(33),
    marginBottom:height(1)
  },
  text:{
    color:"#fff"
  },
  h2:{
    fontSize:totalSize(1.8)
  },
  h3:{
    fontSize:totalSize(1.6)
  },
   conversationContainer:{
     width:width(90),
    //  backgroundColor:"yellow",
     alignSelf:"center",
     marginTop:height(2)
   }
})

export default Chat;