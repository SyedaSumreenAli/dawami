import React from 'react'
import {View,Text, Image, StyleSheet, TouchableOpacity} from 'react-native';
import {height, width, totalSize} from 'react-native-dimension';

const SendMessage = props => {
    return(
        <View style={style.container}>
            <View style={style.thumbnailContainer}>
                <Image 
                style={style.thumbnail}
                source={require('assets/images/person1.png')} 
                />

            </View>
            <View style={style.msgContainer}>
                <View style={style.msgBox}>
                    <Text style={style.msg}>
                    Hello! nice to meet you.
                    Hello! nice to meet you.
                   
                    </Text>
                </View>

                <View>
                    <Text style={style.time}>2:95am</Text>
                </View>
            </View>
        </View>
    )
}


const style = StyleSheet.create({

    container:{
        // backgroundColor:"pink",
        flex:2,
        flexDirection:"row",
        marginVertical:height(2)
    },
    thumbnail:{
        // height:height(4)
    },
    time:{
        color:"#d0d0d0",
        fontSize:totalSize(1.6),
        marginTop:height(2)
    },
    msgBox:{
        backgroundColor:"#8b9b98",
        flex:1,
        // alignSelf:"center",
        width:width(65),
        justifyContent:"center",
        borderRadius:width(2),
        padding:width(2),
      
    },
    thumbnailContainer:{
        flex:1
    },
    msgContainer:{
        flex:4,
        

    },
    msg:{
        color:"#cad4d3"
    }
})

export default SendMessage