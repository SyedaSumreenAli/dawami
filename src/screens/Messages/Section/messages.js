import React from 'react';
import {Text, Block, Button} from 'components';
import * as icons from 'assets/icons';
import {Image} from 'react-native';
import {sizes, colors} from 'styles/theme';


const Messages = (props) =>{
  // console.log(props)
  const { name, time, unreadMessages} = props
  // console.log(name)
  return (
    <Button
      center
      row
     
       onPress={()=> props.navigation.navigate('Chat',{chatWith:name})}
      style={{
        borderBottomWidth: 0.7,
        borderBottomColor: colors.gray,
        height: '100%',
      }}>
      <Block>
        <Image
          source={icons.qnbLogo}
          style={{
            resizeMode: 'contain',
            width: sizes.getWidth(12),
            marginRight: sizes.getWidth(4),
          }}
        />
      </Block>
      <Block
        flex={4}
        //   style={{borderWidth: 1}}
      >
        <Text h2 color={colors.gray2}>
          {name}
        </Text>
      </Block>

      <Block middle crossRight style={{height: '100%'}}>
        {unreadMessages !== null ? (
          <Block
          center middle
            flex={false}
            style={{
              //   borderWidth: 1,
              height: sizes.withScreen(0.025),
              width: sizes.withScreen(0.025),
              borderRadius: sizes.withScreen(3),
              backgroundColor: colors.brown,
              textAlign: 'center',
            }}>
            <Text h3 color={colors.primary}>
              {unreadMessages}
            </Text>
          </Block>
        ) : (
          <Text h3 color={colors.gray}>
            {time} min
          </Text>
        )}
      </Block>
    </Button>
  );
};

export default Messages;
