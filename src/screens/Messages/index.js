import React from 'react';
import {Block, Text, Button, CustomInput} from 'components';
import {colors, sizes} from 'styles/theme';
import * as icons from 'assets/icons';
import {FlatList} from 'react-native';
import {inbox} from 'utils';
import Messages from './Section/messages';
const Message = ({navigation}) => {
  
  return (
   
    <Block>
      {/* header */}
      <Block flex={false} height={sizes.getHeight(12)}>
        <Block center middle style={{backgroundColor: colors.darkBrown}}>
          <Text color={colors.primary} bold h2>
            Conversations
          </Text>
        </Block>
        <Block padding={[0, sizes.getWidth(5)]} center middle flex={2}>
          <Button
            style={{
              width: '100%',
              borderColor: colors.gray,
              borderWidth: 1,
              borderRadius: sizes.getWidth(5),
             
            }}>
            <CustomInput
              placeholder={'Search Company'}
              source={icons.search}
              imgStyling={{tintColor: colors.brown, height: sizes.getHeight(5)}}
            />
          </Button>
        </Block>
      </Block>
      {/* list */}
      <Block flex={6}>
        <FlatList
        
        showsHorizontalScrollIndicator={false}
        showsVerticalScrollIndicator={false}
          data={inbox}
          renderItem={({item}) => {
            return (
              <Block
                middle
                height={sizes.getHeight(9)}
                margin={[sizes.getHeight(0.3), 0, 0, 0]}
                padding={[0, sizes.getWidth(5)]}
                // style={{borderWidth: 1}}
                >
                <Messages {...item} navigation={navigation}/>
              </Block>
            );
          }}
        />
      </Block>
    </Block>
  );
};

export default Message;
