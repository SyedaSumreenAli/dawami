import {Login} from 'screens/Login';
import {Signup} from 'screens/SignUp';
import {WelcomeScreen} from 'screens/WelcomeScreen/Welcome';
import {WelcomingScreen} from 'screens/WelcomeScreen/Welcoming';
import HomePage from './Home';
import {TrainingCatalogue} from 'screens/TrainingCatelogue.js';
import {Companies} from 'screens/Compaines';
import CatalogueDetails from 'screens/CatalogueDetails';
import CompanyPorfolio from 'screens/CompanyProfile';
import JobDescription from 'screens/JobDescriptions';
import JobList from 'screens/jobs';
import Messages from 'screens/Messages';
import Chat from 'screens/Messages/Section/chat';
import Profile from 'screens/UserProfile';
import {EditProfile} from 'screens/UserProfile/Section/EditProfile';
import Contact from 'screens/Contact';
import About from 'screens/About';
import ForgetPassword from 'screens/ForgetPassword';
import Favorites from 'screens/Favorites';
import {CvBuilder} from 'screens/CvBuilder'

export {
  Login,
  Signup,
  WelcomeScreen,
  WelcomingScreen,
  HomePage,
  TrainingCatalogue,
  Companies,
  CatalogueDetails,
  CompanyPorfolio,
  JobDescription,
  JobList,
  Messages,
  Profile,
  EditProfile,
  Contact,
  About,
  ForgetPassword,
  Favorites,
  Chat,
  CvBuilder
};
