import React, {useState, useRef, useEffect} from 'react';
import {
  Block,
  Text,
  CustomHeader,
  Button,
  CustomModal,
  CustomAlert,
  ActivitySign,
} from 'components';
import {colors, sizes} from 'styles/theme';
import {FlatList, Image, Picker, StyleSheet} from 'react-native';
import {ResumeStatus} from './Sections/ResumeStatus';
import {ProfileStatus} from './Sections/ProfileStatus';
import * as icons from 'assets/icons';
import {cvData} from 'utils/dummyData';
import Toast from 'react-native-easy-toast';
import {CVBuilderCards} from './Sections/cvBuilderCard';
import {getAllResumeReuqests} from 'redux/actions/getRequests';
import {useSelector, useDispatch} from 'react-redux';
import {USER_DRAFTS} from 'redux/constants';
import {SubHeading} from './Sections/MainPageSection/heading';

const CvBuilder = ({route, navigation}) => {
  const {userBasicProfile} = useSelector(state => state.auth);
  console.log(userBasicProfile);
  console.log("hi");
  const {allDrafts} = useSelector(state => state.getLists);
  const {status} = useSelector(state => state.auth);
  const dispatch = useDispatch();
  const [FQscreen, setFQscreen] = useState(true);
  const [isWaiting, setIsWaiting] = useState(false);
  const [modalVisibility, setModalVisibility] = useState(false);
  const [btnDisable, setBtnDisable] = useState(true);
  const messageRef = useRef();
  const comingStatus = message => {
    messageRef.current?.show(
      <CustomAlert textColor={colors.customRed} text={message} />,
      4000,
    );
  };

  useEffect(() => {
    status && getAllRequest();
  }, [userBasicProfile.id]);

  // const [submittedReq, setSubmittedReq] = useState();
  const [errorMessage, setErrorMessage] = useState(false);
  let data = {};
  const getAllRequest = async () => {
    setIsWaiting(true);
    const result = await getAllResumeReuqests(userBasicProfile.id, err => {
      messageRef.current?.show(
        <CustomAlert
          textColor={colors.customRed}
          text="We found error in connectivity. please check your internet first "
        />,
        4000,
      ),
        setIsWaiting(false),
        setErrorMessage(true),
        setBtnDisable(true);
    });
    result &&
      (dispatch({type: USER_DRAFTS, payload: result}),
      setBtnDisable(false),
      setIsWaiting(false));
    setBtnDisable(false);
  };

  return (
    <Block>
      <CustomHeader bg={colors.customRed} navigation={navigation} />
      <SubHeading />

      <Block flex={12}>
        <Block>
          <Block flex={9} color={colors.customRed}>
            <Block center middle>
              {status ? (
                allDrafts && allDrafts.length > 0 ? (
                  <FlatList
                    keyExtractor={(item, index) => {
                      return index.toString();
                    }}
                    showsVerticalScrollIndicator={false}
                    data={allDrafts}
                    renderItem={({item}) => {
                      console.log('All Data -----------------------------')
                      console.log(item)
                      return (
                        <CVBuilderCards
                          isWaiting={status => setIsWaiting(status)}
                          data={item}
                          count = {allDrafts.length}
                        />
                      );
                    }}
                  />
                ) : (
                  <Block center middle>
                    <Text h3 color={colors.primary}>
                      NO REQUEST HAS BEEN SUBMIITTED.
                    </Text>
                  </Block>
                )
              ) : (
                <Text h3 color={colors.customRed}>
                  To Make A Request. {'\n'}Please Login First
                </Text>
              )}
            </Block>
          </Block>

          <Block
            center
            middle
            width={'100%'}
            style={{backgroundColor: colors.primary}}>
            <Button
              disabled={!status || btnDisable}
              onPress={() => setModalVisibility(true)}
              activeOpacity={0.3}
              center
              middle
              style={{
                width: sizes.getWidth(50),
                padding: sizes.getWidth(5),
                borderRadius: sizes.getHeight(0.7),
                // borderWidth: 1,
                backgroundColor:
                  !status || btnDisable ? colors.gray : colors.customRed,
                //   bottom:sizes.getHeight(0),
                borderColor: colors.customRed,
              }}>
              <Text h3 color={colors.primary}>
                Request New Resume
              </Text>
            </Button>
          </Block>
        </Block>
      </Block>
      <Toast
        ref={messageRef}
        style={{
          backgroundColor: colors.primary,
          width: sizes.getWidth(100),
          borderRadius: 2,
        }}
        positionValue={sizes.getDimensions.height}
        fadeInDuration={200}
        fadeOutDuration={100}
        opacity={1}
      />
      {/* MODAL */}
      <CustomModal
        closeModal={() => setModalVisibility(false)}
        animation={'slide'}
        isVisible={modalVisibility}>
        {FQscreen ? (
          <ResumeStatus
            goToNext={() => setFQscreen(false)}
            status={message => comingStatus(message)}
            closeModal={() => setModalVisibility(false)}
          />
        ) : (
          <ProfileStatus
            goToPrev={() => setFQscreen(true)}
            status={({message}) => comingStatus(message)}
            closeModal={() => {
              setModalVisibility(false), setFQscreen(true);
            }}
          />
        )}
      </CustomModal>
      {isWaiting && <ActivitySign />}
    </Block>
  );
};

const styles = StyleSheet.create({
  btnStyle: {
    width: sizes.getWidth(50),
    padding: sizes.getWidth(5),
    borderRadius: sizes.getHeight(0.7),
    borderColor: colors.customRed,
  },
});

export {CvBuilder};
