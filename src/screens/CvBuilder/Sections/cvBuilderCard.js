import React, {useState, useRef, useEffect} from 'react';
import {
  Block,
  Text,
  Button,
  ActivitySign,
  CustomModal,
  CustomAlert,
} from 'components';
import {sizes, colors} from 'styles/theme';
import {checkProgress, checkResponse} from 'redux/actions/getRequests';
import {SingleRecord} from './SingleRecord';
import Toast from 'react-native-easy-toast';
import {useDispatch, useSelector} from 'react-redux';
import {CURRENT_VIEW_PROG, ADDING_FEEDBACK} from 'redux/constants';

const CVBuilderCards = props => {
  const {data, isWaiting} = props;

  const dispatch = useDispatch();
  const enumStatus = {
    Pending: '0',
    Assigned: '1',
    InProgress: '2',
    WaitingForFeedBack: '3',
    Completed: '4',
    Cancelled: '5',
    Closed: '6',
    DraftAccepted: '7',
  };

  const getStatus = status => {
    let currentStatus = '';
    switch (status) {
      case '0':
        return (currentStatus = 'Pending');   //0
      case enumStatus.Assigned:
        return (currentStatus = 'Assiged');   // 1
      case enumStatus.InProgress:
        return (currentStatus = 'In-Progress');  //2
      case enumStatus.WaitingForFeedBack:
        return (currentStatus = 'Waiting For FeedBack');  //3
      case enumStatus.Completed:
        return (currentStatus = 'Completed'); //4
      case enumStatus.Cancelled:
        return (currentStatus = 'Cancelled'); //5
      case enumStatus.Closed:
        return (currentStatus = 'Closed'); //6
      case enumStatus.DraftAccepted:
        return (currentStatus = 'Draft Accepted'); //7
      default:
        return (currentStatus = 'Please Wait..'); //8 
    }
  };

  const [modalVisibility, setModalVisiblity] = useState(false);
  // const [draftData, setDraftData] = useState([]);
  const messageRef = useRef();
  const viewProgress = async id => {
    isWaiting(true);
    const result = await checkProgress(
      id,
      err => {
        messageRef.current?.show(
          <CustomAlert text="Request Could not be proceeded" />,
          3500,
        ),
        isWaiting(false)

      }
      )
    if(result){
      console.log(result),
      dispatch({type: CURRENT_VIEW_PROG, payload: result})
      isWaiting(false)
      setModalVisiblity(true)
    }
  };

  console.log(new Date(data.created_at).toLocaleDateString())
  return (
    <Block
      flex={false}
      padding={[sizes.getHeight(1), sizes.getWidth(1)]}
      margin={[sizes.getHeight(0.6), 0]}
      height={sizes.getHeight(25)}
      width={sizes.getWidth('93')}
      color={colors.primary}
      style={{
        elevation: 2,
        borderRadius: sizes.getWidth(0.6),
      }}>
      {/* ==============================================================*/}

      <Block flex={2}>
        <Block padding={[0, sizes.getWidth(3)]} row space={'between'}>
          <Text h3> Status</Text>

          <Block
            center
            middle
            flex={false}
            style={{
              borderWidth: 0.4,
              borderRadius: sizes.getWidth(1),
              width: '40%',
              height: '80%',
            }}>
            {/* <Text h4>{el.status}</Text> */}
            <Text h4>{getStatus(data.status)}</Text>
          </Block>
        </Block>

        <Block padding={[0, sizes.getWidth(3)]} row space={'between'}>
          <Text h4> Created</Text>

          <Block
            center
            middle
            flex={false}
            style={{
              borderWidth: 0.4,
              borderRadius: sizes.getWidth(1),
              width: '40%',
              height: '80%',
            }}>
            <Text h4>{new Date(data.created_at.replace(' ', 'T')).toLocaleDateString()}</Text>
          </Block>
        </Block>
        <Block padding={[0, sizes.getWidth(3)]} row space={'between'}>
          <Text h4> Closed</Text>

          <Block
            center
            middle
            flex={false}
            style={{
              borderWidth: 0.4,
              borderRadius: sizes.getWidth(1),
              width: '40%',
              height: '80%',
            }}>
            <Text h4>{new Date(data.closed_at).toLocaleDateString()}</Text>
          </Block>
        </Block>

        <Block padding={[0, sizes.getWidth(3)]} row space={'between'}>
          <Text h4> Staff Name</Text>

          <Block
            center
            middle
            flex={false}
            style={{
              borderWidth: 0.4,
              borderRadius: sizes.getWidth(1),
              width: '40%',
              height: '80%',
            }}>
            <Text h4>
              {!data.staff ? 'Not Assigned yet' : data.staff.name}
            </Text>
          </Block>
        </Block>

        <Block padding={[0, sizes.getWidth(3)]} row space={'between'}>
          <Text h4> Drafts</Text>

          <Block
            center
            middle
            flex={false}
            style={{
              borderWidth: 0.4,
              borderRadius: sizes.getWidth(1),
              width: '40%',
              height: '80%',
            }}>
            <Text h4>{data.total_drafts}</Text>
          </Block>
        </Block>
      </Block>

      <Block padding={[0, sizes.getWidth(3)]}>
        <Button
          onPress={() => viewProgress(data.id)}
          activeOpacity={0.3}
          style={{
            borderWidth: 0.4,
            borderStyle: 'dashed',
            borderRadius: sizes.getWidth(1),
            borderColor: colors.customRed,
            // width:'30%'
          }}
          center
          middle>
          <Text h3 color={colors.customRed}>
            View Progress
          </Text>
        </Button>

        <Block />
      </Block>

      <CustomModal
        closeModal={() => setModalVisiblity(false)}
        isVisible={modalVisibility}>
        <SingleRecord closeModal={() => setModalVisiblity(false)} />
      </CustomModal>
    </Block>
  );
};

export {CVBuilderCards};
