import React from 'react'
import { Block, Text } from 'components'
import { Image, StyleSheet } from 'react-native'
import { colors, sizes } from 'styles/theme'
import * as icons from 'assets/icons'

const SubHeading = () => {
    return (
        <Block row padding={[0, sizes.padding]} middle>
        <Block center middle>
          <Image
            source={icons.resume}
            style={styles.menuSubLogo}
          />
        </Block>
        <Block flex={9} middle>
          <Text bold h3 color={colors.customRed}>
            Submitted Resume Request Status
          </Text>
        </Block>
      </Block>
    )
}

const styles = StyleSheet.create({
    menuSubLogo:{
        // flex: 0.7,
        width:sizes.getWidth(13),
        tintColor: colors.customRed,
        resizeMode: 'contain',
    },

  })

export {SubHeading}
