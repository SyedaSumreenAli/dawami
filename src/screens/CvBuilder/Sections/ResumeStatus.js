import React, {useState} from 'react';
import {Block, Text, Button, ActivitySign} from 'components';
import {sizes, colors} from 'styles/theme';
import RadioForm, {
  RadioButton,
  RadioButtonInput,
  RadioButtonLabel,
} from 'react-native-simple-radio-button';
import {useSelector} from 'react-redux';
import {requestResume} from 'redux/actions/getRequests';
const ResumeStatus = props => {
  const {userInfo} = useSelector(state => state.userInfo);
  const [isWaiting, setIsWaiting]= useState(false)
  let radio_props = [
    {label: 'Yes It is uploaded', value: 1},
    {label: 'Not Yet', value: 0},
  ];

  const radioHandler = async obj => {
    setIsWaiting(true)
    console.log(userInfo.id);
    if (obj) {
      const result = await requestResume(userInfo.id, err => {
        err && console.log(err);
      });
      result && result.success && props.status(result.message);
      setIsWaiting(false)
      props.closeModal();
    } else {
      setIsWaiting(false)
      props.goToNext();
    }
  };

  return (
    <Block
      center
      middle
      style={{width: '100%', borderRadius: sizes.getWidth(2)}}>
      {/* ============================================================================ */}
      <Block
        flex={false}
        center
        middle
        style={{
          // borderWidth:0.6,
          position: 'absolute',
          elevate: 3,
          zIndex: 3,
          elevation: 7,
          backgroundColor: 'white',
          height: sizes.getHeight(30),
          width: '80%',
          paddingBottom: 20,
          borderRadius: sizes.getWidth(1),
        }}>
        <Block
          margin={[0, 0, sizes.getHeight(2), 0]}
          center
          middle
          style={{
            borderBottomWidth: 0,
            backgroundColor: 'white',
            elevation: 2,
            width: '100%',
          }}>
          <Text color={'green'}>Please Tell us</Text>
        </Block>

        <Block center middle>
          <Text color={colors.gray2} h2>
            Have You Uploaded Your Resume !{' '}
          </Text>
        </Block>

        <Block center middle margin={[20, 0]}>
          {/* ========================================= */}
          <RadioForm
            radio_props={radio_props}
            onPress={obj => radioHandler(obj)}
            buttonColor={colors.customRed}
            selectedButtonColor={colors.customRed}
            buttonOuterSize={25}
            buttonSize={12}
            initial={3}
            formHorizontal={true}
            wrapStyle={{marginLeft:10}}
            labelColor={colors.customRed}
            selectedLabelColor={colors.customRed}
          />
          {/* ========================================= */}
        {isWaiting && <ActivitySign  />}

        </Block>
       
      </Block>
      {/* ============================================================================ */}

      <Button
        onPress={props.closeModal}
        style={{
          // backgroundColor: 'red',
          //   position: 'absolute',
          //   top: 0,
          width: '100%',
          height: '100%',
        }}>
        {/* <Text>123</Text> */}
      </Button>

     
    </Block>
  );
};

export {ResumeStatus};
