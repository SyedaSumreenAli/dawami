import {BasicInfo} from './BasicInfo';
import {ContactInfo} from './ContactInfo';
import {PreferredJobs} from './PreferredJobs';
import {WorkInfo} from './WorkInfo';
import {EducationalInfo} from './EduInfo';
import {Skills} from './SkillInfo';
import {LangInfo} from './LangInfo';
import {CertInfo} from './CertInfo';
import {Hobbies} from './Hobbies';
import {ReferencesInfo} from './References';

export {
  BasicInfo,
  ContactInfo,
  PreferredJobs,
  WorkInfo,
  EducationalInfo,
  Skills,
  LangInfo,
  CertInfo,
  ReferencesInfo,
  Hobbies
};
