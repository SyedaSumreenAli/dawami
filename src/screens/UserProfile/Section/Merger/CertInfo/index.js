import React from 'react';
import {Block, TextField, Text} from 'components';
import {StyleSheet} from 'react-native';
import {sizes, colors} from 'styles/theme';
import {Heading, SubHeading} from '../../Section';
import DatePicker from 'react-native-datepicker';

const CertInfo = () => {
  return (
    <Block
      margin={[0, 0, sizes.getHeight(4), 0]}
      padding={[0, 0, sizes.getHeight(4), 0]}>

      <Block flex={false} row center style={styles.recordKeeperCon}>
        <SubHeading>Type</SubHeading>
        <TextField
          inputStyling={{
            borderBottomWidth: 1,
            width: '100%',
            margin: 0,
            paddingBottom: 0,
          }}
        />
      </Block>

      <Block flex={false} row center style={styles.recordKeeperCon}>
        <SubHeading>Name</SubHeading>
        <TextField
          inputStyling={{
            borderBottomWidth: 1,
            width: '100%',
            margin: 0,
            paddingBottom: 0,
          }}
        />
      </Block>

      <Block flex={false} row center style={styles.recordKeeperCon}>
        <SubHeading>Start Date</SubHeading>

        <DatePicker
          style={{
            width: sizes.getWidth(65),
            // borderWidth: 1,
            justifyContent: 'center',
            height: sizes.getHeight(5),
          }}
          placeholder="Select Date"
          mode="date"
          format="MM-DD-YYYY"
          //   date={dateState}
          // onDateChange={data => setDateState({date:date})}
        />
      </Block>

      <Block flex={false} row style={styles.recordKeeperCon}>
        <SubHeading>Expiry Date</SubHeading>
        <TextField
          inputStyling={{
            borderBottomWidth: 1,
            width: '100%',
            margin: 0,
            paddingBottom: 0,
          }}
        />
      </Block>

      <Block
        center
        flex={false}
        row
        style={{...styles.recordKeeperCon, height: sizes.getHeight(7)}}>
        <SubHeading>Attachment</SubHeading>
        <Block
          middle
          center
          style={{
            // borderWidth: 1,
            backgroundColor:colors.darkBrown,
            borderRadius: sizes.getWidth(2),
            width: '40%',
            height: '80%',
          }}
          flex={false}>
          <Text h4 color={colors.primary}>No Attachment Found</Text>
        </Block>
      </Block>





    </Block>
  );
};

const styles = StyleSheet.create({
  recordKeeperCon: {
    height: sizes.getHeight(5),
    marginBottom: sizes.getHeight(2),
    // borderWidth:1,
  },
});

export {CertInfo};
