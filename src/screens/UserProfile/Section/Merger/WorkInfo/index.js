import React, {useState} from 'react';
import {Block, TextField, Button, Text} from 'components';
import {StyleSheet} from 'react-native';
import {sizes, colors} from 'styles/theme';
import {SubHeading, Heading} from '../../Section';
import {Picker} from '@react-native-community/picker';
import Checkbox from '@react-native-community/checkbox'
import RadioForm, {
  RadioButton,
  RadioButtonInput,
  RadioButtonLabel,
} from 'react-native-simple-radio-button';
import DatePicker from 'react-native-datepicker';

const WorkInfo = () => {
  const [exp, setExp] = useState({value: ''});
  const [selectedGender, setSelectedGender] = useState();
  let radio_props = [
    {label: 'I have Exp', value: 0},
    {label: 'I do not have Exp', value: 1},
  ];
  return (
    <Block padding={[0, 0, sizes.getHeight(2), 0]}>
      <Heading> WORK EXPERIENCE + </Heading>

      <Block
        margin={[sizes.getHeight(2), 0, 0, 0]}
        flex={false}
        row
        center
        style={styles.recordKeeperCon}>
        <Block>
          <RadioForm formHorizontal={true} animation={true}>
            {/* ==================== */}

            {radio_props.map((obj, i) => (
              <RadioButton labelHorizontal={true} key={i}>
                {/*  You can set RadioButtonLabel before RadioButtonInput */}
                <RadioButtonInput
                  obj={obj}
                  //   index={i}
                  //   isSelected={this.state.value3Index === i}
                  onPress={() => alert('1')}
                  borderWidth={1}
                  buttonInnerColor={'#e74c3c'}
                  buttonOuterColor={colors.customRed}
                  // this.state.value3Index === i ? '#2196f3' : '#000'
                  //   }
                  buttonSize={15}
                  buttonOuterSize={18}
                  buttonStyle={{}}
                  buttonWrapStyle={{marginLeft: sizes.getWidth(2)}}
                />
                <RadioButtonLabel
                  obj={obj}
                  index={i}
                  labelHorizontal={true}
                  onPress={() => alert('2')}
                  labelStyle={{fontSize: sizes.h2, color: colors.customRed}}
                  labelWrapStyle={{}}
                />
              </RadioButton>
            ))}
            {/* ======================== */}
          </RadioForm>
        </Block>
      </Block>

      {/* total time */}

      <Block row center style={styles.recordKeeperCon}>
        <SubHeading>Total Time</SubHeading>
        <Block>
          <Picker
            selectedValue={selectedGender}
            onValueChange={(itemValue, index) => setSelectedGender(itemValue)}
            style={{width: sizes.getWidth(40), hieght: sizes.getHeight(5)}}>
            <Picker.Item label="Years" value="CITIZEN" />
            <Picker.Item label="1" value="REST" />
            <Picker.Item label="2" value="REST" />
            <Picker.Item label="3" value="REST" />
            <Picker.Item label="4" value="REST" />
          </Picker>
        </Block>

        <Block>
          <Picker
            selectedValue={selectedGender}
            onValueChange={(itemValue, index) => setSelectedGender(itemValue)}
            style={{width: sizes.getWidth(35), hieght: sizes.getHeight(5)}}>
            <Picker.Item label="Months" value="CITIZEN" />
            <Picker.Item label="1" value="REST" />
            <Picker.Item label="2" value="REST" />
            <Picker.Item label="3" value="REST" />
            <Picker.Item label="4" value="REST" />
          </Picker>
        </Block>
      </Block>

      <Block row center style={styles.recordKeeperCon}>
        <SubHeading>Job Title</SubHeading>
        <Block flex={9}>
          <TextField inputStyling={{borderBottomWidth: 1, width: '100%'}} />
        </Block>
        <Button center middle style={{width: '10%'}}>
          <Text> X </Text>
        </Button>
      </Block>

      <Block row center style={styles.recordKeeperCon}>
        <SubHeading>JOB CATEGORY</SubHeading>
        <Block>
          <Picker
            selectedValue={selectedGender}
            onValueChange={(itemValue, index) => setSelectedGender(itemValue)}
            style={{width: sizes.getWidth(60), hieght: sizes.getHeight(5)}}>
            <Picker.Item label="Select Any Category" value="CITIZEN" />
            <Picker.Item label="BANKING" value="REST" />
            <Picker.Item label="MANAGEMENT" value="REST" />
            <Picker.Item label="SECURITY" value="REST" />
          </Picker>
        </Block>
      </Block>

      <Block row center style={styles.recordKeeperCon}>
        <SubHeading>Country</SubHeading>
        <Block>
          <Picker
            selectedValue={selectedGender}
            onValueChange={(itemValue, index) => setSelectedGender(itemValue)}
            style={{width: sizes.getWidth(60), hieght: sizes.getHeight(5)}}>
            <Picker.Item label="Select Country" value="CITIZEN" />
            <Picker.Item label="PAKSITAN" value="REST" />
            <Picker.Item label="QATAR" value="REST" />
          </Picker>
        </Block>
      </Block>

      <Block row center style={styles.recordKeeperCon}>
        <SubHeading>Country</SubHeading>
        
        <Block>
          <DatePicker
            style={{
              width: sizes.getWidth(35),
              // borderWidth: 1,
              justifyContent: 'center',
              height: sizes.getHeight(5),
            }}
            placeholder="Select Date"
            mode="date"
            format="MM-DD-YYYY"
            // date={dateState}
            // onDateChange={data => setDateState({date:date})}
          />
        </Block>

        <Block>
          <DatePicker
            style={{
              width: sizes.getWidth(35),
              // borderWidth: 1,
              justifyContent: 'center',
              height: sizes.getHeight(5),
            }}
            placeholder="Select Date"
            mode="date"
            format="MM-DD-YYYY"
            // date={dateState}
            // onDateChange={data => setDateState({date:date})}
          />
        </Block>
      </Block>


      <Block row center>
          <Checkbox  />
          <Text h3>I Still Work Here</Text>
          </Block>


        <Block row>
            <SubHeading>Description</SubHeading>
            <Block>
            <TextField multilines={true} numberOfLines={5} inputStyling={{borderWidth:0.6, width:'100%'}} />      
            </Block>
        </Block>

        <Block row margin={[sizes.getHeight(2),0,0,0]}>
            <SubHeading>Company Name</SubHeading>
            <Block>
            <TextField inputStyling={{borderWidth:0.6, width:'100%'}} />      
            </Block>
        </Block>

        <Block row margin={[sizes.getHeight(2),0,0,0]}>
            <SubHeading>Company Industry</SubHeading>
            <Block>
            <TextField inputStyling={{borderWidth:0.6, width:'100%'}} />      
            </Block>
        </Block>

        <Block row margin={[sizes.getHeight(2),0,0,0]}>
            <SubHeading>Salary Of Last Month</SubHeading>
            <Block>
            <TextField keyboardType={'numeric'} inputStyling={{borderWidth:0.6, width:'100%'}} />      
            </Block>
        </Block>









    </Block>
  );
};

const styles = StyleSheet.create({
  recordKeeperCon: {
    height: sizes.getHeight(5),
    marginBottom: sizes.getHeight(2),
  },
});

export {WorkInfo};
