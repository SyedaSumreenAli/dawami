import React from 'react';
import {Picker} from '@react-native-community/picker';
import {StyleSheet} from 'react-native';
import {sizes, colors} from 'styles/theme';
import { color } from 'react-native-reanimated';

const VisaSelection = props => {
  const {visaChange, currentVisaType} = props
  // console.log('===props===visa===');
  // console.log(props);
  const GV = {
    CNTRY: {
      PAK: {name: 'PAKISTAN', value: '1'},
      QTR: {name: 'QATAR', value: '2'},
    },
    RC: {
      CTZ: {name: 'Citizen', value: '1'},
      RVT: {name: 'Residence Visa (Transferable)', value: '2'},
      RVNT: {name: 'Residence Visa (Non-Transferable)', value: '3'},
      STD: {name: 'Student', value: '4'},
      TV: {name: 'Transit Visa', value: '5'},
      VV: {name: 'Visit Visa', value: '6'},
      NV: {name: 'No Visa', value: '7'},
    },
  };
  return (
    <Picker
      selectedValue={props.el}
      onValueChange={(itemValue, index) => visaChange(itemValue,index)}
      style={styles.pickerStyle}>
      <Picker.Item label={GV.RC.CTZ.name} value={GV.RC.CTZ.value} />
      <Picker.Item label={GV.RC.RVT.name} value={GV.RC.RVT.value} />
      <Picker.Item label={GV.RC.RVNT.name} value={GV.RC.RVNT.value} />
      <Picker.Item label={GV.RC.STD.name} value={GV.RC.STD.value} />
      <Picker.Item label={GV.RC.TV.name} value={GV.RC.TV.value} />
      <Picker.Item label={GV.RC.VV.name} value={GV.RC.VV.value} />
      <Picker.Item label={GV.RC.NV.name} value={GV.RC.NV.value} />
    </Picker>
  );
};

const styles = StyleSheet.create({
  pickerStyle: {
    width: sizes.getWidth(40),
    transform: [{scaleX: 0.8}, {scaleY: 0.8}],
    height: sizes.getHeight(5),
    backgroundColor:'#EEEEEE99'
  },
  recordKeeperCon: {
    height: sizes.getHeight(12),
    // borderWidth:1,
    width: '100%',
    // backgroundColor: 'red',
    // marginBottom: sizes.getHeight(2),
  },
});
export {VisaSelection};
