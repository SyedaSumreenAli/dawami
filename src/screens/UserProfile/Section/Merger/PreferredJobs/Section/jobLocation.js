import React, {useState} from 'react';
import {Block, Text, Button} from 'components';
import {SubHeading} from 'screens/UserProfile/Section/Section';
import {Picker} from '@react-native-community/picker';
import {sizes, colors} from 'styles/theme';
import {StyleSheet} from 'react-native';
import {VisaSelection} from './visa';

const JobLocation = props => {
  const {onPress, showBtn, sign} = props;
  // console.log('=====props========');
  // console.log(props);
  const GV = {
    CNTRY: {
      PAK: {name: 'PAKISTAN', value: '1'},
      QTR: {name: 'QATAR', value: '2'},
    },
    RC: {
      CTZ: {name: 'Citizen', value: '1'},
      RVT: {name: 'Residence Visa (Transferable)', value: '2'},
      RVNT: {name: 'Residence Visa (Non-Transferable)', value: '3'},
      STD: {name: 'Student', value: '4'},
      TV: {name: 'Transit Visa', value: '5'},
      VV: {name: 'Visit Visa', value: '6'},
      NV: {name: 'No Visa', value: '7'},
    },
  };

  return (
    <Picker
      selectedValue={props.el}
      onValueChange={(itemValue, index) =>
        props.countryChange(itemValue, index)
      }
      style={styles.pickerStyle}>
      <Picker.Item label={GV.CNTRY.PAK.name} value={GV.CNTRY.PAK.value} />
      <Picker.Item label={GV.CNTRY.QTR.name} value={GV.CNTRY.QTR.value} />
    </Picker>
  );
};

const styles = StyleSheet.create({
  pickerStyle: {
    width: sizes.getWidth(40),
    transform: [{scaleX: 0.8}, {scaleY: 0.8}],
    height: sizes.getHeight(5),
    backgroundColor:'#EEEEEE99'
  },
  recordKeeperCon: {
    height: sizes.getHeight(12),
    // borderWidth:1,
    width: '100%',
    // backgroundColor: 'red',
    // marginBottom: sizes.getHeight(2),
  },
});

export {JobLocation};
