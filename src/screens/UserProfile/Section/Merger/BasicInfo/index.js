import React, {useState, useReducer, useRef, useEffect} from 'react';
import {Heading, SubHeading} from '../../Section';
import {TextField, Block, Text, Button, ActivitySign} from 'components';
import {sizes, colors} from 'styles/theme';
import {StyleSheet, useColorScheme, ActivityIndicator} from 'react-native';
import {Picker} from '@react-native-community/picker';
// import DateTimePicker from '@react-native-community/datetimepicker'
import DatePicker from 'react-native-datepicker';
import {useSelector, useDispatch} from 'react-redux';
import {UpdateUserInfo} from 'redux/actions';
import Toast from 'react-native-easy-toast';

const BasicInfo = props => {
  const {message,showWaiting} = props
  const GV = {
    //generic values+ initial values
    username: '',
    randomVal: null,
    Male: {name: 'Male', value: '1'},
    Female: {name: 'Femlae', value: '2'},
    PAK: {name: 'PAKISTAN', value: '1'},
    QTR: {name: 'QATAR', value: '2'},
    RC: {
      CTZ: {name: 'Citizen', value: '1'},
      RVT: {name: 'Residence Visa (Transferable)', value: '2'},
      RVNT: {name: 'Residence Visa (Non-Transferable)', value: '3'},
      STD: {name: 'Student', value: '4'},
      TV: {name: 'Transit Visa', value: '5'},
      VV: {name: 'Visit Visa', value: '6'},
      NV: {name: 'No Visa', value: '7'},
    },
    MRD: {name: 'Married', value: '1'},
    UMMRD: {name: 'Un-Married', value: '2'},
  };

  const {user,profile} = useSelector(state => state.userInfo.userData );

  const DISPATCH = useDispatch()
  // VISA SELECTION
  const visaType = visaId => {
    switch (visaId) {
      case GV.RC.CTZ.value:
        return GV.RC.CTZ.value;
      case GV.RC.RVT.value:
        return GV.RC.RVT.value;
      case GV.RC.RVNT.value:
        return GV.RC.RVNT.value;
      case GV.RC.STD.value:
        return GV.RC.STD.value;
      case GV.RC.TV.value:
        return GV.RC.TV.value;
      case GV.RC.VV.value:
        return GV.RC.VV.value;
      case GV.RC.NV.value:
        return GV.RC.NV.value;
      default:
        return GV.randomVal;
    }
  };

  console.log("=====================================")
  console.log(profile.license_country)
  console.log("=====================================")

  const initialState = {
    disableSaveBtn: true,
    name: profile.full_name,
    dob: profile.date_of_birth || GV.randomVal,
    gender: profile.gender || GV.randomVal,
    nationality: profile.nationality_id || GV.randomVal,
    residentCountry: profile.residence_id || GV.randomVal,
    visaStatus: visaType(profile.visa_status),
    maritalStatus: profile.marital_status || GV.randomVal,
    numberOfDependent: profile.no_of_dependents || '0',
    licenseCountry: profile.license_country || GV.randomVal,
  };
  // GET TODAY DATE ONLY
  let todayDate = new Date()
    .toJSON()
    .slice(0, 10)
    .replace("/-/g,'/'")
    .toString();

  const reducer = (state, action) => {
    
    switch (action.type) {
      case 'EditName':
        return {
          ...state,
          name: action.payload,
          disableSaveBtn: action.payload === user.name,
        };
      //==========================GENERAL
      case 'genderSelection':
        return {
          ...state,
          gender: action.payload,
          disableSaveBtn: action.payload === profile.gender,
        };
      case 'changeDate':
        if (action.payload >= todayDate) {
          message('Please enter a valid DOB')
          setIsWaiting(false)
        } else {
          return {
            ...state,
            dob: action.payload,
            disableSaveBtn: action.payload === profile.date_of_birth,
          };
        }
      case 'nationality':
        return {
          ...state,
          nationality: action.payload,
          disableSaveBtn: action.payload === profile.nationality_id,
        };
      case 'residenceCountry':
        return {
          ...state,
          residentCountry: action.payload,
          disableSaveBtn: action.payload === profile.country_id,
        };
      case 'visaStatus':
        return {
          ...state,
          visaStatus: action.payload,
          disableSaveBtn: action.payload === profile.visa_status,
        };
      case 'maritalStatus':
        return {
          ...state,
          maritalStatus: action.payload,
          disableSaveBtn: action.payload === profile.marital_status,
        };
      case 'numberOfDependents':
        return {
          ...state,
          numberOfDependent: action.payload,
          disableSaveBtn: action.payload === profile.no_of_dependents,
        };
      case 'licenseCountry':
        return {
          ...state,
          licenseCountry: action.payload,
          disableSaveBtn: action.payload === profile.license_country,
        };
      case 'onSave' : 
      return {...state, disableSaveBtn:true}
      default:
        return state;
    }
  };
  const inputRef = useRef();
  const [state, dispatch] = useReducer(reducer, initialState);
  const [isWaiting, setIsWaiting] = useState(false);

  const inputHandler = ({text, name}) => {
    console.log(name);
    switch (name) {
      case 'name':
        console.log(text);
        return dispatch({type: 'EditName', payload: text});
      case 'numberOfDependents':
        console.log(text);
        dispatch({type: 'numberOfDependents', payload: text});
      default:
        console.log('default');
        return;
    }
  };

  const saveInfoHandler = async () => {
    dispatch({type:'onSave'})
    showWaiting(true)
    setIsWaiting(true)
    const updatedDetails = {
      user_id: user.id,
      full_name: state.name,
      date_of_birth: state.dob,
      gender: state.gender,
      nationality_id: state.nationality,
      visa_status: state.visaStatus,
      residence_id: state.residentCountry,
      marital_status: state.maritalStatus,
      no_of_dependents: state.numberOfDependent,
      license_country: state.licenseCountry,
    }
    const result = await UpdateUserInfo(updatedDetails, err => {
      console.log('err'),
      message('Unable To Saved. Please Try again later..')
      showWaiting(false)
      setIsWaiting(false)

    })
    result && (
    // need to dispatch
      // console.log(result),
      DISPATCH(result),
      message('Thanks ! Record has been updated'),
      setIsWaiting(false)

    )
    showWaiting(false)
    setIsWaiting(false)
  };

  // let data = [{value: 'Male'}, {value: 'Female'}];
  console.log('current state');
  console.log(state);
  return (
    <Block
      style={{borderWidth: 0, elevation: 3, backgroundColor: 'white'}}
      padding={[sizes.padding]}>
      {/* name */}
      <Block flex={false} row center style={styles.recordKeeperCon}>
        <SubHeading> Name</SubHeading>
        <Block padding={[0, 0, sizes.getHeight(1), 0]}>
          <TextField
            value={state.name}
            inputRef={inputRef}
            name={'name'}
            onChangeText={inputHandler}
            inputStyling={{
              borderBottomWidth: 1,
              width: '100%',
              margin: 0,
              paddingBottom: 0,
            }}
          />
        </Block>
      </Block>
      {/* DOB */}
      <Block flex={false} row center style={styles.recordKeeperCon}>
        <SubHeading> Date Of Birth</SubHeading>
        <DatePicker
          style={{
            width: sizes.getWidth(65),
            justifyContent: 'center',
            height: sizes.getHeight(5),
          }}
          placeholder="Select Date"
          mode="date"
          format="YYYY-MM-DD"
          date={state.dob}
          onDateChange={date => {
            dispatch({type: 'changeDate', payload: date});
          }}
        />
      </Block>
      {/* gender */}
      <Block row center style={styles.recordKeeperCon}>
        <SubHeading>Gender</SubHeading>
        <Block style={{backgroundColor: '#EEEEEE'}}>
          <Picker
            selectedValue={state.gender}
            onValueChange={(itemValue, index) =>
              dispatch({type: 'genderSelection', payload: itemValue})
            }
            style={styles.pickerStyle}>
            <Picker.Item label={GV.Male.name} value={GV.Male.value} />
            <Picker.Item label={GV.Female.name} value={GV.Female.value} />
          </Picker>
        </Block>
      </Block>
      {/* nationality */}
      <Block row center style={{...styles.recordKeeperCon}}>
        <SubHeading>Nationnality</SubHeading>
        <Block style={{backgroundColor: '#EEEEEE'}}>
          <Picker
            selectedValue={state.nationality}
            onValueChange={(itemValue, index) =>
              dispatch({type: 'nationality', payload: itemValue})
            }
            style={styles.pickerStyle}>
            <Picker.Item label={GV.PAK.name} value={GV.PAK.value} />
            <Picker.Item label={GV.QTR.name} value={GV.QTR.value} />
          </Picker>
        </Block>
      </Block>
      {/* residence country */}
      <Block row center style={styles.recordKeeperCon}>
        <SubHeading>Residence Country</SubHeading>
        <Block style={{backgroundColor: '#EEEEEE'}}>
          <Picker
            selectedValue={state.residentCountry}
            onValueChange={(itemValue, index) =>
              dispatch({type: 'residenceCountry', payload: itemValue})
            }
            style={styles.pickerStyle}>
            <Picker.Item label={GV.PAK.name} value={GV.PAK.value} />
            <Picker.Item label={GV.QTR.name} value={GV.QTR.value} />
          </Picker>
        </Block>
      </Block>
      {/* visa status */}
      <Block row center style={{...styles.recordKeeperCon}}>
        <SubHeading>Visa Status</SubHeading>
        <Block style={{backgroundColor: '#EEEEEE'}}>
          <Picker
            selectedValue={state.visaStatus}
            onValueChange={(itemValue, index) =>
              dispatch({type: 'visaStatus', payload: itemValue})
            }
            style={styles.pickerStyle}>
            <Picker.Item label={GV.RC.CTZ.name} value={GV.RC.CTZ.value} />
            <Picker.Item label={GV.RC.RVT.name} value={GV.RC.RVT.value} />
            <Picker.Item label={GV.RC.RVNT.name} value={GV.RC.RVNT.value} />
            <Picker.Item label={GV.RC.STD.name} value={GV.RC.STD.value} />
            <Picker.Item label={GV.RC.TV.name} value={GV.RC.TV.value} />
            <Picker.Item label={GV.RC.VV.name} value={GV.RC.VV.value} />
            <Picker.Item label={GV.RC.NV.name} value={GV.RC.NV.value} />
          </Picker>
        </Block>
      </Block>
      {/* merital status */}
      <Block row center style={styles.recordKeeperCon}>
        <SubHeading>Marital Status</SubHeading>
        <Block style={{backgroundColor: '#EEEEEE'}}>
          <Picker
            selectedValue={state.maritalStatus}
            onValueChange={(itemValue, index) =>
              dispatch({type: 'maritalStatus', payload: itemValue})
            }
            style={styles.pickerStyle}>
            <Picker.Item label={GV.MRD.name} value={GV.MRD.value} />
            <Picker.Item label={GV.UMMRD.name} value={GV.UMMRD.value} />
          </Picker>
        </Block>
      </Block>
      {/* number of dependents */}
      <Block row center style={styles.recordKeeperCon}>
        <SubHeading>Number Of Dependents</SubHeading>
        <Block>
          <TextField
            value={state.numberOfDependent}
            maxLength={1}
            onChangeText={inputHandler}
            name={'numberOfDependents'}
            keyboardType="numeric"
            inputStyling={{borderBottomWidth: 1, width: '100%'}}
          />
        </Block>
      </Block>
      {/* license Country */}
      <Block row center style={styles.recordKeeperCon}>
        <SubHeading>License Country</SubHeading>
        <Block style={{backgroundColor: '#EEEEEE'}}>
          <Picker
            selectedValue={state.licenseCountry}
            onValueChange={(itemValue, index) =>
              dispatch({type: 'licenseCountry', payload: itemValue})
            }
            style={styles.pickerStyle}>
            <Picker.Item label={GV.PAK.name} value={GV.PAK.value} />
            <Picker.Item label={GV.QTR.name} value={GV.QTR.value} />
          </Picker>
        </Block>
      </Block>

      <Block
        bottom
        crossRight
        flex={false}
        height={sizes.getHeight(8)}
        // style={{borderWidth: 1}}
      >
        <Button
          onPress={saveInfoHandler}
          disabled={isWaiting || state.disableSaveBtn}
          activeOpacity={0.6}
          center
          middle
          style={{
            borderRadius: sizes.getWidth(1),
            backgroundColor:
              state.disableSaveBtn || isWaiting
                ? colors.gray
                : colors.customRed,
            width: '40%',
          }}>
          {isWaiting ? (
            <ActivityIndicator size={'large'} color={colors.primary} />
          ) : (
            <Text color={colors.primary}>Save</Text>
          )}
        </Button>
      </Block>
      <Block />

    </Block>
  );
};

const styles = StyleSheet.create({
  pickerStyle: {
    width: sizes.getWidth(70),
    transform: [{scaleX: 0.8}, {scaleY: 0.8}],
    height: sizes.getHeight(5),
    // alignSelf: 'stretch',
    // flex: 1,
  },
  recordKeeperCon: {
    height: sizes.getHeight(5),
    marginBottom: sizes.getHeight(1),
  },
});

export {BasicInfo};
