import React, {useState, useRef} from 'react';
import {Text, Block, TextField, Button} from 'components';
import {Heading, SubHeading} from '../../Section';
import {StyleSheet, TextInput, ActivityIndicator} from 'react-native';
import {sizes, colors} from 'styles/theme';
import {useSelector, useDispatch} from 'react-redux';
import { UpdateContactInfo } from 'redux/actions';

const ContactInfo = props => {
  const dispatch = useDispatch()
  const {message,showWaiting} = props
  const phMinLength = 11
  const phMaxLength = 15
  const error_message = 'Unable to Save. Please try Again Later...'
  const feedback = 'Great ! Phone Number is Up To Date'

  const inputHandler = ({name, text}) => {
    text.length >= phMinLength ? (
      setDisableSaveButton(false),
      setPhone(text)
      ):(
        setDisableSaveButton(true),
        setPhone(text)
      )
  };

  
  const {user,profile} = useSelector(state => state.userInfo.userData);
  console.log('=========================================')
  console.log(profile)
  console.log('=========================================')

  // console.log(userInfo);
  const inputRef = useRef();
  const [phone, setPhone] = useState(profile?.phone);
  const [isWaiting, setIsWaiting] = useState(false);
  const [disableSaveBtn, setDisableSaveButton] = useState(true);

  // console.log(phone);

  const saveInfoHandler = async() => {
    const {id} = user
    // console.log(id)
    showWaiting(true)
    const result = await UpdateContactInfo({id,phone}, err => {
      message(error_message)
      showWaiting(false)
      setDisableSaveButton(true)
    })
    result && (
      // need to dispatch
      console.log('result--before dispatch'),
      console.log(result),
      dispatch(result),
      // dispatch(result),
      console.log(result),
      showWaiting(false),
      message(feedback),
      setDisableSaveButton(true)
    )
  };

  return (
    <Block margin={[0, 0, sizes.getHeight(3), 0]}>
      <Block flex={false} row center style={styles.recordKeeperCon}>
        <SubHeading> Name</SubHeading>
        <Block bottom padding={[0, sizes.getWidth(2)]}>
          <Block flex={false} style={{borderBottomWidth: 1, borderBottomColor:colors.gray}}>
            <Text
              h3
              color={colors.gray}
              style={{textTransform: 'capitalize', marginBottom: 5}}>
              {user.name}
            </Text>
          </Block>
        </Block>
      </Block>

      {/* phone */}

      <Block flex={false} row center style={styles.recordKeeperCon}>
        <SubHeading>Contact</SubHeading>
        <Block padding={[0, sizes.getWidth(2)]}>
          <TextField
            inputRef={inputRef}
            onChangeText={inputHandler}
            name={'ph'}
            maxLength={phMaxLength}
            keyboardType={'numeric'}
            placeholder={
              profile.phone ? '' : 'Please Add Phone Number'
            }
            value={phone}
            inputStyling={{
              borderBottomWidth: 1,
              width: '100%',
              margin: 0,
              paddingBottom: 8,
              color:colors.gray3,
              borderBottomColor:colors.gray
            }}
          />
        </Block>
      </Block>

      <Block
        bottom
        crossRight
        flex={false}
        height={sizes.getHeight(8)}
        padding={[0, sizes.padding]}
        // style={{borderWidth: 1}}
      >
        <Button
          onPress={saveInfoHandler}
          disabled={isWaiting || disableSaveBtn}
          activeOpacity={0.6}
          center
          middle
          style={{
            borderRadius: sizes.getWidth(1),
            backgroundColor:
              disableSaveBtn || isWaiting ? colors.gray : colors.customRed,
            width: '40%',
          }}>
          {isWaiting ? (
            <ActivityIndicator size={'large'} color={colors.primary} />
          ) : (
            <Text color={colors.primary}>Save</Text>
          )}
        </Button>
      </Block>
    </Block>
  );
};

const styles = StyleSheet.create({
  recordKeeperCon: {
    marginTop: sizes.getHeight(2),
    height: sizes.getHeight(5),
    marginBottom: sizes.getHeight(2),
    //   borderWidth:1
  },
});

export {ContactInfo};
