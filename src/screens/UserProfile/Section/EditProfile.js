import React, {useState, useRef, forwardRef, useEffect} from 'react';
import {
  Block,
  Text,
  Button,
  TextField,
  CustomAlert,
  ActivitySign,
} from 'components';
import {sizes, colors} from 'styles/theme';
import {
  ScrollView,
  Image,
  TextInput,
  TouchableOpacity,
  Animated,
  StyleSheet,
  Alert,
} from 'react-native';
import * as images from 'assets/images';
import * as icons from 'assets/icons';
import Accordian from 'react-native-collapsible/Accordion';
import {
  ContactInfo,
  PreferredJobs,
  BasicInfo,
  WorkInfo,
  EducationalInfo,
  Skills,
  LangInfo,
  CertInfo,
  ReferencesInfo,
  Hobbies,
} from './Merger';
import Animator from '../Animator';
import Toast from 'react-native-easy-toast';
import {useSelector, useDispatch} from 'react-redux';
import {GetProfileInfo} from 'redux/actions';

const EditProfile = props => {
  const {navigation} = props;
  const dispatch = useDispatch();
  const {userBasicProfile} = useSelector(state => state.auth);
  const {userData} = useSelector(state => state.userInfo);
  console.log(userData);
  console.log("a");
   console.log(userBasicProfile.id);
  const compRef = React.createRef();
  const [viewBasicInfo, setViewBasicInfo] = useState(false);

  const messageRef = useRef();

  const [showMessage, setShowMessage] = useState(false);
  const [message, setMessage] = useState('');
  const [isWaiting, setIsWaiting] = useState(false);

  // const slideDown = useRef(new Animated.Value(0)).current
  // useEffect(() => {
  //   Animated.timing(
  //      slideDown,
  //        {
  //         useNativeDriver:true,
  //         toValue:1,
  //         duration:700
  //     }

  //   ).start()
  // })
  // console.log('messageRef.current')
  // console.log(messageRef.current)

  const messageHandler = message => {
    messageRef.current?.show(<CustomAlert text={message} />, 3500);
  };
  const ActivityHandler = status => {
    // console.log(status)
    setIsWaiting(status);
  };

  useEffect(() => {
    async function fetchProfileRecords() {
      setIsWaiting(true);
      const result = await GetProfileInfo(userBasicProfile.id, err => {
        return (
          console.log('error'),
          setIsWaiting(false),
          Alert.alert(
            'Error In Fetch Records',
            'Sorry We Cannot Go Further Please Check You Internet Connectivity first',
            [
              {text:'Go To Profile', onPress:()=> navigation.navigate('Profile',{fetchError:true} )},
              {text:'Back To Home', onPress:()=> navigation.navigate('Home',{fetchError:true})},
            ]
          )
        );
      });
      result && (dispatch(result), setIsWaiting(false));
      setIsWaiting(false);
    }
    if (!userData?.success) {
      fetchProfileRecords();
    }
  },[]);

  return (
    <Block middle>
      <Block
        center
        middle
        flex={false}
        height={sizes.getHeight(25)}
        style={{backgroundColor: 'transparent', elevation: 1, width: '100%',}}>
        <Block
          center
          middle
          flex={false}
          height={sizes.withScreen(0.1)}
          width={sizes.withScreen(0.1)}
          style={{overflow: 'hidden', borderRadius: sizes.withScreen(3)}}>
          <Image
            source={images.person}
            style={{resizeMode: 'contain', flex: 1}}
          />
        </Block>
        <Button
          center
          middle
          style={{
            //   borderWidth:1,
            position: 'absolute',
            bottom: sizes.getHeight(7),
            right: sizes.getWidth(36),
            height: sizes.screenSize * 0.024,
            width: sizes.screenSize * 0.024,
            borderRadius: sizes.screenSize * 3,
            backgroundColor: colors.customRed,
          }}>
          <Image
            source={icons.camera}
            style={{
              resizeMode: 'contain',
              width: sizes.getWidth(4),
            }}
          />
        </Button>

        {/* ============================ */}
        <Block center middle flex={false}>
          <Text h3 color={colors.customRed}>
            Ibrahim Muhammad
          </Text>
          <Text h4 color={colors.customRed}>
            UI / UX Designer
          </Text>
        </Block>
      </Block>
      <Block padding={[0, sizes.getWidth(2)]}>
        {/* ================================================ */}
        <ScrollView
          style={{
            width: '100%',
            paddingTop: sizes.getHeight(3),
            // borderWidth:1,
          }}
          showsVerticalScrollIndicator={false}>
          <Animator title="Basic Info">
            <BasicInfo
              message={message => messageHandler(message)}
              showWaiting={status => ActivityHandler(status)}
            />
          </Animator>

          <Animator title="Contact Info">
            <ContactInfo
              message={message => messageHandler(message)}
              showWaiting={status => ActivityHandler(status)}
            />
          </Animator>

          <Animator title="Preffered Jobs">
            <PreferredJobs
              message={message => messageHandler(message)}
              showWaiting={status => ActivityHandler(status)}
            />
          </Animator>

          <Animator title="Education">
            <EducationalInfo 
             message={message => messageHandler(message)}
             showWaiting={status => ActivityHandler(status)}
             />
          </Animator>

          <Animator title="Skills">
            <Skills />
          </Animator>

          <Animator title="Languages">
            <LangInfo />
          </Animator>

          <Animator title="Certifications">
            <CertInfo />
          </Animator>

          <Animator title="References">
            <ReferencesInfo />
          </Animator>

          <Animator title="Hobbies">
            <Hobbies />
          </Animator>

          {/* <ContactInfo />
          <PreferredJobs />
          <WorkInfo />
          <EducationalInfo />
          <Skills />
          <LangInfo />
          <CertInfo />
          <ReferencesInfo />
          <Hobbies/> */}
        </ScrollView>
      </Block>

      <Toast
        ref={messageRef}
        style={{
          backgroundColor: colors.customRed,
          width: sizes.getWidth(100),
          borderRadius: 2,
        }}
        positionValue={sizes.getDimensions.height}
        fadeInDuration={200}
        fadeOutDuration={100}
        opacity={1}
      />

      {isWaiting && <ActivitySign />}
    </Block>
  );
};

const styles = StyleSheet.create({
  headingStyle: {
    // borderWidth:1,
    height: sizes.getHeight(7),
    justifyContent: 'center',
    borderRadius: sizes.getWidth(1),
    paddingHorizontal: sizes.getWidth(2),
    backgroundColor: colors.primary,
  },
});

export {EditProfile};
