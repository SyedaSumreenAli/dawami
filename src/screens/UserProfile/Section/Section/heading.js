import React from 'react'
import { Block, Text } from 'components'
import { Image } from 'react-native'
import * as icons from 'assets/icons'
import { colors, sizes } from 'styles/theme'

const Heading = props => {
    return (
       <Block row  
    //    flex={false}
    //    style={{borderWidth:1}}
       >
           <Block flex={false} width={sizes.getWidth(10)}  middle>
           <Image source={icons.white_user} style={{resizeMode:'contain',width:sizes.getWidth(5), tintColor:colors.black}} />
           </Block>
           <Block middle flex={4}>
           <Text h3 bold>{props.children}</Text>
           </Block>
           </Block>
    )
}

export  {Heading}
