import React from 'react';
import {Block, Text, Button} from 'components';
import * as icons from 'assets/icons';
import {Image} from 'react-native';
import {colors, sizes} from 'styles/theme';

export const WelcomingScreen = ({navigation}) => {
  return (
    <Block center middle style={{backgroundColor: colors.customRed}}>
      <Block flex={false} middle>
        <Image
          source={icons.white_logo}
          style={{resizeMode: 'contain', width: sizes.getWidth(47)}}
        />
      </Block>
      <Block
        flex={false}
        middle
        row
        style={{width: '35%', justifyContent: 'space-between'}}>
        <Button onPress={() => {navigation.navigate('Home')}} center middle style={{width: '40%'}}>
          <Image
            source={icons.english_word}
            style={{resizeMode: 'contain', width: sizes.getWidth('13')}}
          />
        </Button>
        <Button
        onPress={() => {navigation.navigate('Home')}}
          middle
          center
          style={{width: '40%', paddingBottom: sizes.getHeight(1)}}>
          <Image
            source={icons.arabi_word}
            style={{resizeMode: 'contain', width: sizes.getWidth('9')}}
          />
        </Button>
      </Block>
    </Block>
  );
};
