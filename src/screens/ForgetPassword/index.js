import React, {useState, useReducer, useRef, useEffect} from 'react';
import {
  Block,
  Text,
  Button,
  TextField,
  CustomModal,
  ActivitySign,
  CustomAlert,
} from 'components';
// import {TextInput, ActivityIndicator} from 'react-native';
import {sizes, colors} from 'styles/theme';
import {ForgetPasswordFormState, validateField, LoginFormState} from 'utils';
import {validateFields} from 'utils/Validations';
import Toast from 'react-native-easy-toast';
import {forgetPassword, verifyCode} from 'redux/actions';
import {useDispatch, useSelector, useStore} from 'react-redux';
import CodeVerification from './Section/verficationCode';
import NewPasswordScreen from './Section/setNewPassword';
import {REMOVE_FORGET_TOKEN} from 'redux/constants';
import { Keyboard } from 'react-native';
const ForgetPassword = ({navigation}) => {
  const alertTiming = 2500
  // const localActions
  const SET_STORED_TOKEN = 'SET_STORE_TOKEN';
  const SET_MODAL_VISIBILITY = 'SET_MODAL_VISIBILITY';
  const SET_PASSWORD_SCREEN_VISIBILITY = 'SET_PASSWORD_SCREEN_VISIBILITY';
  const ACTIVITY_INDICATOR = 'ACTIVITY_INDICATOR';

  const initialState = {
    storedToken: null,
    modalVisibility: false,
    atPasswordScreen: false,
    activityIndicator: false,
  };
  const reducer = (state, action) => {
    switch (action.type) {
      case SET_STORED_TOKEN:
        return {...state, storedToken: action.payload};
      case SET_MODAL_VISIBILITY:
        return {...state, modalVisibility: action.payload};
      case ACTIVITY_INDICATOR:
        return {...state, activityIndicator: action.payload};
      case SET_PASSWORD_SCREEN_VISIBILITY:
        return {...state, atPasswordScreen: action.payload};
      default:
        return state;
    }
  };

  const messageRef = useRef();
  const authSection = useSelector(state => state.auth);
  const {reset_confirmation_code, requestedUserDetails} = authSection;
  const dispatch = useDispatch();

  const [localState, localDispatch] = useReducer(reducer, initialState);
  console.log(localState);
  const [validations, setValidations] = useState({
    fields: {...ForgetPasswordFormState},
    sendBtnDisable: false,
  });
  const handleInput = ({name, text}) => {
    const newFields = validateField(validations.fields[name], text);
    setValidations({fields: {...validations.fields, [name]: newFields}});
  };
  const handleSubmit = async () => {
    Keyboard.dismiss()
    const fields = validateFields(validations.fields);
    // console.log('sTATE user')
    // console.log(validations.fields.email.value)
    if (fields.validity) {
      if (
        localState.storedToken !== null &&
        reset_confirmation_code !== null &&
        requestedUserDetails.email !== null &&
        requestedUserDetails.email === validations.fields.email.value
      ) {
        //token already in store
        console.log('i am here in already in store token state')
        localDispatch({type: ACTIVITY_INDICATOR, payload: true});
        localDispatch({type: SET_MODAL_VISIBILITY, payload: true});
        localDispatch({type: ACTIVITY_INDICATOR, payload: false});
      } else {
        //token is not in store
        !initialState.activityIndicator &&
          localDispatch({type: ACTIVITY_INDICATOR, payload: true});
        console.log('NEW REQUEST FOR GET CONFIRMATION CODE');
        const result = await forgetPassword(
          validations.fields.email.value,
          err => (
            console.log('==========ERROR IN FORGET======'),
            console.log(err),
            err.message ? 
            messageRef.current?.show(
             <CustomAlert text={err.message} />,alertTiming
            ):
            messageRef.current?.show(
              <CustomAlert text="Please Check Internet Connectivity" />, alertTiming
            ),
            console.log('result not ook'),
            localDispatch({type: ACTIVITY_INDICATOR, payload: false})
          )
        )
        result &&
          (console.log('result is ook'),
            console.log(result),
          dispatch(result),
          localDispatch({
            type: SET_STORED_TOKEN,
            payload: result.payload.code.confirmation_code,
          }),
          localDispatch({type: SET_MODAL_VISIBILITY, payload: true}),
          localDispatch({type: ACTIVITY_INDICATOR, payload: false}));
      }
      // fields are not completed
    } else {
      // alert('Please Fill Required Fields');
      messageRef.current?.show(
          // {validations.fields.email.errorMessage}
        <CustomAlert text={validations.fields.email.errorMessage} />,alertTiming
      );
    }
    setValidations({fields: fields.newFields})
    // navigation.navigate('Login', {resetCompleted:true})
  };
  useEffect(() => {
    // messageRef.current?.show();
    console.log('FORGET PASSWORD HAS RERENDERED ')
  },[localState,navigation]);
  const [codeStatus,setCodeStatus] = useState(false)
  // CALL THE API TO RESET PASSWORD
  const handleValidCode = status => {
    if (status) {
      console.log('Code MATCHED');
      localDispatch({type: ACTIVITY_INDICATOR, payload: true});
      dispatch({type: REMOVE_FORGET_TOKEN});
      localDispatch({type: SET_PASSWORD_SCREEN_VISIBILITY, payload: true});
      localDispatch({type: ACTIVITY_INDICATOR, payload: false});
      console.log(authSection);
    } else {
      setCodeStatus(true)
    }
  };

  const onBackPress = () => {
    console.log('on back pressed state');
    console.log(localState);
    localState.atPasswordScreen
      ? (dispatch({type: REMOVE_FORGET_TOKEN}),
        localDispatch({type: SET_STORED_TOKEN, payload: null}),
        localDispatch({type: SET_MODAL_VISIBILITY, payload: false}),
        localDispatch({type: SET_PASSWORD_SCREEN_VISIBILITY, payload: false}))
      : localDispatch({type: SET_MODAL_VISIBILITY, payload: false});
  };

  const resetComplete = () => {
    console.log('DONE RESET');
    localDispatch({type: SET_MODAL_VISIBILITY, payload: false});
    navigation.navigate('Login', {resetCompleted:true})
  };

  return (
    <Block middle center padding={[0, sizes.getWidth(5)]}>
      <Block flex={false} height={sizes.getHeight(10)} row>
        <Block flex={4} middle>
          <TextField
            keyboardType="email-address"
            inputStyling={{
              borderWidth: 0.4,
              // elevation:1,
              width: '100%',
              borderRadius: sizes.getWidth(0.06),
            }}
            name={'email'}
            onChangeText={handleInput}
            placeholder="Type Your Register @email"
          />
        </Block>

        <Block middle>
          <Button
            disable={validations.sendBtnDisable}
            onPress={handleSubmit}
            
            middle
            center
            style={{
              // elevation:5,
              backgroundColor: validations.sendBtnDisable
                ? colors.gray
                : colors.customRed,
              height: sizes.getHeight(7),
            }}>
            <Text color={colors.primary}>Send</Text>
          </Button>
        </Block>
      </Block>
      <Block flex={false}>
        <Text h4 color={colors.gray} style={{textAlign: 'center'}}>
          <Text h4 color={colors.gray2} bold>
            Instructions:{' '}
          </Text>
          An email will be send for confirmation to this email which has been
          typing after click the send button, an confirmation button will be
          appear after once you get email, please click to get verified, we will
          send a new password to your registered email{' '}
          <Text h4 bold color={colors.customRed} style={{textAlign: 'center'}}>
            {'\n'}
            Please Follow these instructions {'\n'}
          </Text>
          {/* Or Click Below link to get reset you password by visiting */}
        </Text>
        {/* <Button
          onPress={() => {
            alert(
              'Sorry for inconvenience ! We are working on it yet update in next update',
            );
          }}
          center>
          <Text h4 color={colors.customRed}>
            https://wedigit.com/forget-password
          </Text>
        </Button> */}
      </Block>

      <Block flex={false} style={{width: '100%', height: sizes.getHeight(10)}}>
        <Toast
          ref={messageRef}
          style={{
            backgroundColor: colors.customRed,
            width: sizes.getWidth(100),
            borderRadius: 0,
            marginBottom: 40,
          }}
          positionValue={sizes.getHeight(5)}
          fadeOutDuration={2500}
          opacity={0.8}
        />
      </Block>

      <CustomModal
        closeModal={onBackPress}
        isVisible={localState.modalVisibility}>
        {!localState.atPasswordScreen ? (
          <CodeVerification
            codeStatus={codeStatus}
            isValid={handleValidCode}
            compareWithCode={`${(localState.storedToken &&
              localState.storedToken) ||
              authSection.reset_confirmation_code}`}
          />
        ) : (
          <NewPasswordScreen
            unmountMe={() =>
              localDispatch({
                type: SET_PASSWORD_SCREEN_VISIBILITY,
                payload: false,
              })
            }
            resetCompleted={resetComplete}
          />
        )}
      </CustomModal>

      {localState.activityIndicator && <ActivitySign />}

      <Toast
        ref={messageRef}
        style={{
          backgroundColor: colors.customRed,
          width: sizes.getWidth(100),
          borderRadius: 0,
        }}
        positionValue={sizes.getDimensions.height}
        fadeInDuration={300}
        fadeOutDuration={100}
        opacity={1}
      />
    </Block>
  );
};

export default ForgetPassword;
