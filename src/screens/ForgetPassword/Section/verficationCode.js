import CodeInput from 'react-native-confirmation-code-input';
import React, {useRef, useEffect} from 'react';
import {Block, Text, ActivitySign} from 'components';
import {sizes, colors} from 'styles/theme';
import Toast from 'react-native-easy-toast'

const CodeVerification = props => {
  const {isValid,codeStatus,indicatorStatus} = props;

  

  return (
    <Block
      middle
      center
      flex={false}
      padding={[sizes.getHeight(3), 0]}
      style={{
        // borderWidth: 1,
        borderRadius: sizes.withScreen(0.004),
        backgroundColor: colors.primary,
        elevation: 5,
      }}
      height={sizes.getHeight(25)}
      width={sizes.getWidth(70)}>
      <Text color={colors.customRed}>Verification Code.</Text>
      <Text
        color={colors.customRed}
        style={{fontSize: sizes.customFont(9), textAlign: 'center'}}>
        A Code has been sent to given email.{'\n'}Please type that code below to
        verify yourself.
      </Text>
      <CodeInput
        // ref={inputRef}
        className={'border-b-t'}
        space={sizes.getWidth(1)}
        size={sizes.getHeight(5)}
        activeColor={colors.lightPink}
        inactiveColor={colors.primary}
        codeLength={6}
        cellBorderWidth={2}
        keyboardType="number-pad"
        codeInputStyle={{
          fontWeight: 'bold',
          color: colors.customRed,
          borderBottomColor: colors.gray,
        }}
        blurOnSubmit={true}
        ref={props.ref}
        onFulfill={status => isValid(status)}
        {...props}
      />
       {codeStatus && <Text h4 color={colors.customRed}>Wrong Code Please Try Again.</Text>}
      {indicatorStatus && <ActivitySign />}
    </Block>
  );
};

export default CodeVerification
