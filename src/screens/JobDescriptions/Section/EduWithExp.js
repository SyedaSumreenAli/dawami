import React from 'react';
import {Block, Text} from 'components';
import {Image, ScrollView} from 'react-native';
import * as icons from 'assets/icons';
import * as images from 'assets/images';
import {sizes, colors} from 'styles/theme';

const EduWithExp = () => {
  return (
    <Block
      padding={[sizes.getWidth(2), sizes.getWidth(5), 0, sizes.getWidth(5)]}>
      <Block margin={[sizes.getHeight(1), 0, 0, 0]}>
        <Text h4 bold>
          Education & Experience
        </Text>
        <Text
          padding={[10, 0]}
          style={{fontSize: sizes.customFont(10), lineHeight: 18}}>
          I have been waiting. I’ve been waiting all day.
          {'\n'}
          Waiting for Gus to send one of his men to kill me. And it’s you. Who
          do you know, who’s okay with using children, Jesse? Who do you know…
          who’s allowed children to be murdered… hmm? Gus! He has, he has been
          ten steps ahead of me at every turn.
        </Text>
      </Block>
    </Block>
  );
};

export default EduWithExp;
