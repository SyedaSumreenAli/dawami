import React from 'react';
import {Block, Text, CustomHeader, Button} from 'components';
import {colors, sizes} from 'styles/theme';
import {Image, StyleSheet} from 'react-native';
import * as icons from 'assets/icons';

const PortfolioHeader = () => {
  return (
    <Block flex={1.5} style={{backgroundColor: colors.primary}}>
      {/* <CustomHeader /> */}
      <Block
        padding={[10, sizes.getWidth(5), 0, sizes.getWidth(5)]}
        row
        flex={3}
        // style={{borderWidth: 1, backgroundColor: 'red'}}
      >
        <Block flex={3}>
          <Text h2 bold>
            Customer Credit Analyst
          </Text>
          <Text h4 style={{fontWeight: 'bold'}}>
            Qatar Gass
          </Text>
          <Text h4>Doha, Qatar - Full Time</Text>
          <Text h4>Posted 1 Month Ago - 210 applications</Text>
        </Block>
        <Block center middle>
          <Image
            source={icons.qnbListLogo}
            style={{resizeMode: 'contain', flex: 0.5}}
          />
        </Block>
      </Block>

      <Block
        padding={[0, sizes.getWidth(5)]}
        row
        center
        flex={3}
        >
        <Block middle margin={[0, sizes.getWidth(3), 0, 0]}>
          <Button
            center
            middle
            style={{
              height: sizes.getHeight(4),
              width: sizes.getWidth(8),
              borderWidth: 1,
              borderColor: colors.gray,
              borderRadius: sizes.withScreen(0.5),
            }}>
            <Image
              source={icons.empty_heart}
              style={{resizeMode: 'contain', flex: 0.4}}
            />
          </Button>
        </Block>
        <Block middle flex={9}>
          <Button
            center
            middle
            style={{
              borderRadius: sizes.withScreen(0.09),
              backgroundColor: colors.customRed,
              width: sizes.getWidth(40),
            }}>
            <Text h3 color={colors.primary}>
              Apply for this job
            </Text>
          </Button>
        </Block>
      </Block>
    </Block>
  );
};
export default PortfolioHeader;
