import React from 'react';
import {Block, Text, Button} from 'components';
import {DrawerContentScrollView, DrawerItem} from '@react-navigation/drawer';
import * as icons from 'assets/icons';
import {sizes, colors} from 'styles/theme';
import {Image, StyleSheet} from 'react-native';
import * as images from 'assets/images';
import DrawerItems from './drawerItem';
import DrawerInfo from './DrawerInfo';
import {useSelector, useDispatch} from 'react-redux';
import { logout } from 'redux/actions';

const DrawerContent = props => {
  // console.log(props);
  const authSection = useSelector(state => state.auth);
  const {status} = authSection;
  const dispatch = useDispatch()
  return (
    <Block padding={[0, sizes.getWidth(5)]}>
      <DrawerContentScrollView {...props}>
        {/* <DrawerInfo /> */}
        <DrawerItems
          route={props.navigation}
          label="Profile"
          to={'Profile'}
          image={icons.white_user}
        />
        <DrawerItems
          route={props.navigation}
          label="Message"
          to={'Messages'}
          image={icons.message}
        />
        <DrawerItems
          route={props.navigation}
          label="Favorites"
          to={'Favorites'}
          image={icons.empty_heart}
        />
        <DrawerItems
          route={props.navigation}
          to={'Company'}
          label="Company"
          image={icons.company}
        />
        <DrawerItems
          route={props.navigation}
          to={'Job'}
          label="Jobs"
          image={icons.job}
        />
        <DrawerItems
          route={props.navigation}
          to={'Training Catalogue'}
          label="Training Catalogue"
          image={icons.training_catalog}
        />
        <DrawerItems
          route={props.navigation}
          label="Contact Us"
          to={'Contact'}
          image={icons.company}
        />
        <DrawerItems
          route={props.navigation}
          label="About"
          to={'About'}
          image={icons.company}
        />
      </DrawerContentScrollView>
      <DrawerItem
        icon={({color, size}) => (
          <Image source={icons.logout} style={styles.iconStyle} />
        )}
        style={styles.listStyle}
        label="Logout"
        onPress={()=> dispatch(logout())}
        labelStyle={styles.labelStyle}
      />
    </Block>
  );
};

const styles = StyleSheet.create({
  labelStyle: {
    // borderWidth: 1,
    marginHorizontal: -sizes.getHeight(3),
    fontSize: sizes.h2,
  },
  listStyle: {
    // borderWidth: 1,
    height: sizes.getHeight(7),
    justifyContent: 'center',
  },
  iconStyle: {
    tintColor: colors.gray,
    width: sizes.getWidth(4),
    resizeMode: 'contain',
  },
});

export default DrawerContent;
