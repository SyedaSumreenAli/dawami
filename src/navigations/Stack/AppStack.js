import React from 'react';
import {
  CompanyPorfolio,
  HomePage,
  Companies,
  Contact,
  About,
  JobList,
  JobDescription,
  TrainingCatalogue,
  CatalogueDetails,
  WelcomeScreen,
  WelcomingScreen,
  Profile,
  EditProfile,
  Messages,
  Chat,
  Favorites,
  ForgetPassword,
  Login,
  Signup,
  CvBuilder
} from 'screens';
import {createStackNavigator} from '@react-navigation/stack';
import {CustomHeader} from 'components';
import {colors} from 'styles/theme';

const AppStack = ({route, navigation}) => {
  const AppNavigation = createStackNavigator();
  return (
    // <AppNavigation.Navigator initialRouteName="Home" headerMode="none">
      <AppNavigation.Navigator initialRouteName="Home" headerMode="none">
      <AppNavigation.Screen name="Home" component={HomePage} />
      <AppNavigation.Screen name="Company" component={Companies} />
      <AppNavigation.Screen
        name="CVBuilder"
        component={CvBuilder}
        options={{headerShown: false}}
      />
      <AppNavigation.Screen name="Job" component={JobList} />
      <AppNavigation.Screen name="jobDetails" component={JobDescription} />
      <AppNavigation.Screen
        name="Training Catalogue"
        component={TrainingCatalogue}
      />
      <AppNavigation.Screen
        name="details"
        component={CatalogueDetails}
        options={{headerShown: false}}
      />

      <AppNavigation.Screen
        name="Contact"
        component={Contact}
        // options={{
        //   header: props => <CustomHeader bg={colors.brown} {...props} />,
        // }}
      />
      <AppNavigation.Screen
        name="About"
        component={About}
        // options={{
        //   header: props => <CustomHeader bg={'transparent'} {...props} />,
        // }}
      />
      <AppNavigation.Screen
        name="AboutCompany"
        component={CompanyPorfolio}
        // options={{
        //   header: props => (
        //     <CustomHeader
        //       bg={colors.brown}
        //       noLogo
        //       backBtn
        //       heartBtn
        //       {...props}
        //     />
        //   ),
        // }}
      />

      {/* LOGIN */}
      <AppNavigation.Screen
        name="Welcome"
        component={WelcomeScreen}
        options={{headerShown: false}}
      />
      <AppNavigation.Screen
        name="AgainWelcome"
        component={WelcomingScreen}
        options={{headerShown: false}}
      />
      <AppNavigation.Screen
        name="ForgetPassword"
        component={ForgetPassword}
        options={{headerShown: false}}
        // options={{
        //   header: props => (
        //     <CustomHeader bg={colors.customRed} backBtn {...props} />
        //   ),
        // }}
      />
      <AppNavigation.Screen
        name="Login"
        component={Login}
        options={{headerShown: false}}
      />
      <AppNavigation.Screen
        name="Signup"
        component={Signup}
        options={{headerShown: false}}
      />
      {/* LOGIN END */}

      {/* USER ROUTES */}

      <AppNavigation.Screen
        name="Profile"
        component={Profile}
        options={{
          header: props => (
            <CustomHeader backBtn bg={colors.customRed} {...props} />
          ),
        }}
      />
      <AppNavigation.Screen
        name="EditProfile"
        component={EditProfile}
        options={{
          header: props => (
            <CustomHeader backBtn bg={colors.customRed} {...props} />
          ),
        }}
      />
      <AppNavigation.Screen
        name="Messages"
        component={Messages}
        options={{
          header: props => <CustomHeader bg={colors.darkBrown} {...props} />,
        }}
      />
      <AppNavigation.Screen
        name="Chat"
        component={Chat}
        options={{
          header: props => <CustomHeader bg={colors.darkBrown} {...props} />,
        }}
      />
      <AppNavigation.Screen
        name="Favorites"
        component={Favorites}
        options={{
          header: props => <CustomHeader backBtn {...props} />,
        }}
      />
    </AppNavigation.Navigator>
  );
};

export {AppStack};
