// import {AuthStack} from './Stack/AuthStack'
import {UserStack} from './Stack/UserStack';
import {AppStack} from './Stack/AppStack';

import {Navigations} from './MainNavigation';

export default Navigations;
export {
    // AuthStack, 
    UserStack, AppStack};
