import React from 'react'
import { Block, Text } from 'components'
import { sizes, colors } from 'styles/theme'
import { Image, TextInput } from 'react-native'
import * as icons from 'assets/icons'
import Button from './Button'

const Searchbar = props => {
    const { title, placeholder,bg } = props
    return (
        <Block
            padding={[0, sizes.padding * 1.5]}
            flex={false} height={sizes.getHeight(15)}
            style={{
                // borderWidth: 1, 
                backgroundColor: bg ? bg :colors.brown
            }}
        >
            <Block flex={false} height={sizes.getHeight(6)} center>
                <Text h2 color={colors.primary}>
                    {!title && "Enter Title please"}
                    {title}
                </Text>
            </Block>
            <Block
                row center flex={false}
                style={{
                    backgroundColor: colors.primary,
                    borderRadius: sizes.withScreen(0.004),
                    height: sizes.getHeight(6)
                }}>

                <Image
                    source={icons.search}
                    style={{
                        tintColor: colors.gray, resizeMode: 'contain',
                        width: sizes.getWidth(8),
                        //  backgroundColor: 'red', 
                        height: '100%',
                    }} />
                <TextInput
                    placeholder={placeholder || "Please Enter Place holder"}
                    style={{
                        width: '78%',
                        // borderWidth: 1
                    }} />

                <Button center middle style={{
                    // borderWidth: 1,
                    width: '13%'
                }}>
                    <Image source={icons.filter} style={{
                        resizeMode: 'contain', width: sizes.getWidth(6),

                    }} />
                </Button>

            </Block>
        </Block>
    )
}

export default Searchbar
