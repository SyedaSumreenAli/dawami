import React, { useEffect, useState } from 'react';
import {Block, Text, Button} from 'components';
import {sizes, colors} from 'styles/theme';
import {Image} from 'react-native';
import * as icons from 'assets/icons';
import * as images from 'assets/images';
import {useSelector, useDispatch} from 'react-redux';
import { LANG_EN,LANG_AR } from 'redux/constants';
import { language } from 'redux/actions/user';

export const CustomHeader = (props) => {
  const {status} = useSelector(state => state.auth);
  const {bg, backBtn, heartBtn, noLogo, tint, navigation} = props;
  const dispatch = useDispatch()
  const {currentLN} = useSelector(state=> state.userInfo)

  const [initialLN,setInitialLN] = useState(LANG_EN)
  // language controller===================
  const languageHandler = () => {
    if(currentLN === LANG_EN){
      // set to arabic
      console.log('its english')
      const langResult = language(LANG_AR)
      dispatch(langResult)
    }
    else{
      console.log(' not en')
      const langResult = language(LANG_EN)
      dispatch(langResult)
    }
  }

  return (
    <Block
      space={'between'}
      center
      row
      middle
      flex={false}
      height={sizes.getHeight(10)}
      style={{backgroundColor: bg ? bg : colors.primary}}>
      <Block
        center
        middle
        // style={{borderWidth: 1}}
      >
        {/* ==========LEFT BUTTON============== */}
        {status &&
        <Button
          center
          middle
          style={{borderWidth:0, width:'100%'}}
          onPress={() => {
            backBtn ? navigation.goBack() : navigation.openDrawer();
          }}>
          {backBtn ? (
            <Image
              source={icons.goBack}
              style={{
                tintColor: bg
                  ? bg !== colors.primary
                    ? tint
                      ? tint
                      : colors.primary
                    : tint
                    ? tint
                    : colors.primary
                  : tint
                  ? tint
                  : colors.darkBrown,
                resizeMode: 'contain',
                width: sizes.getWidth(2),
              }}
            />
          ) : (
            <Image
              source={icons.menu}
              style={{
                tintColor: bg
                  ? bg !== colors.primary
                    ? tint
                      ? tint
                      : colors.primary
                    : colors.darkBrown
                  : tint
                  ? tint
                  : colors.primary,
                resizeMode: 'contain',
                width: sizes.getWidth(6),
              }}
            />
          )}
        </Button>
        }
         {/* LEFT BUTTON ENDED */}
      </Block>
      <Block center middle flex={5}>
        {!noLogo && (
          <Image
            source={icons.dawamiOne}
            style={{
              resizeMode: 'contain',
              flex: 0.7,
              // tintColor: bg ? colors.customRed : colors.darkBrown,
              tintColor: bg
                ? bg !== colors.primary
                  ? tint
                    ? tint
                    : colors.primary
                  : tint
                  ? tint
                  : colors.darkBrown
                : tint
                ? tint
                : colors.darkBrown,
            }}
          />
        )}
      </Block>
      <Block center middle>
        <Button
          onPress={() => {
            heartBtn
              ? alert('item add into favorites')
              : languageHandler();
          }}
          center
          middle>
          {heartBtn ? (
            <Image
              source={icons.empty_heart}
              style={{
                tintColor: bg
                  ? bg !== colors.primary
                    ? colors.primary
                    : colors.darkBrown
                  : colors.darkBrown,
                resizeMode: 'contain',
                width: sizes.getWidth(5),
              }}
            />
          ) : (
            <Text
              // color={bg ? colors.customRed : colors.Black}
              color={
                bg
                  ? bg !== colors.primary
                    ? tint
                      ? tint
                      : colors.primary
                    : // : tint?tint:colors.darkBrown
                      'green'
                  : tint
                  ? tint
                  : colors.darkBrown
              }>
              {currentLN}
            </Text>
          )}
        </Button>
      </Block>
    </Block>
  );
};
