import Block from './Block';
import Typography from './Text';
import Searchbar from './Searchbar';
import Button from './Button';
import {SquareButton} from './SquareButton';
import {CustomHeader} from './CustomHeader';
import {CustomInput} from './CustomInput';
import {CustomModal} from './Modal';
import ChatButton from './ChatButton';
import TextField from './TextField';
import SimpleToast from './SimpleToast';
import ActivitySign from './ActivityIndicator'
import PopupVerification from './PopupVerification'
import {CustomAlert} from './AlertError'
import{TextFieldOne} from './TextFieldOne'

export {
  Typography as Text,
  Block,
  Button,
  SquareButton,
  CustomHeader,
  CustomInput,
  Searchbar,
  ChatButton,
  TextField,
  SimpleToast,
  CustomModal,
  ActivitySign,
  PopupVerification,
  CustomAlert,
  TextFieldOne
};
