import React from 'react';
import {Block, Button, Text} from 'components';
import {sizes, colors} from 'styles/theme';
import {Image} from 'react-native';
import * as icons from 'assets/icons';

export const SquareButton = props => {
  const {
    children,
    height,
    width,
    textColor,
    bgColor,
    withImage,
    noBorder,
    borderRadius,
  } = props;
  return withImage ? (
    <Button
      {...props}
      center
      middle
      style={{
        borderWidth: !bgColor ? 1 : 0,
        borderColor: !bgColor ? colors.gray : 'transparent',
        height: height || sizes.getHeight('6.5%'),
        width: width || '100%',
        backgroundColor: bgColor || 'transparent',
        borderRadius: sizes.withScreen(0.003),
        flexDirection: 'row',
        ...props.style,
      }}>
      <Image
        source={props.source}
        style={{
          resizeMode: 'contain',
          width: sizes.getWidth(4),
          marginHorizontal: sizes.getWidth(1),
        }}
        {...props.imgSyling}
      />
      <Text
        style={{
          color: textColor || colors.black,
          marginHorizontal: sizes.getWidth(1),
        }}>
        {children}
      </Text>
    </Button>
  ) : (
    <Button
      center
      middle
      {...props}
      style={{
        zIndex: 1,
        borderWidth: !bgColor ? (noBorder ? 0 : 1) : 0,
        height: height || sizes.getHeight('6.5%'),
        width: width || sizes.getWidth('90%'),
        backgroundColor: bgColor || 'transparent',
        borderRadius: sizes.withScreen(0.003),
        ...props.style,
      }}>
      <Text style={{color: textColor || colors.black}}>{children}</Text>
    </Button>
  );
};
