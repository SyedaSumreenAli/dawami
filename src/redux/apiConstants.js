export const baseUrl = "https://wedigits.dev/api/";

const ImageBasePath = 'https://wedigits.dev/images/';
export const companyLogo = `${ImageBasePath}company_logos/`

export const Registration = `${baseUrl}register`
export const LoginUser = `${baseUrl}login`
export const ForgetPasswordSendEmail = `${baseUrl}reset/password/sendEmail`
export const ForgetPasswordVerifyCode = `${baseUrl}reset/password/verify`
export const ResetPassword = `${baseUrl}reset/password`
export const GetCompanies = `${baseUrl}companies`
export const GetJobs = `${baseUrl}jobs`


export const Google = `${baseUrl}google-login`      // params email + name
export const Facebook = `${baseUrl}facebook-login`  // params email + name


export const CheckUserName = `${baseUrl}check/username/`
export const CheckEmail = `${baseUrl}check/email/`
export const RemoveEmail = `${baseUrl}remove/user/email/`
export const UserVerfication = `${baseUrl}verify/token`

// ==========
export const resumeRequest = `${baseUrl}request/resume/`
export const seeAllResumeRequest = `${baseUrl}requests/all/`
export const viewProgress = `${baseUrl}request/resume/progress/`
export const draftLike = `${baseUrl}request/resume/liked/`
export const draftDislike = `${baseUrl}request/resume/disliked/draft`
export const checkResponse = `${baseUrl}request/resume/draft/response/`


// User Profile
export const ProfileInfo = `${baseUrl}user/`
export const BasicInfoUpdate = `${baseUrl}profile/`