import AsyncStorage from '@react-native-community/async-storage';
import {combineReducers, applyMiddleware, createStore} from 'redux';
import {authReducer, userInfoReducer, getRequestReducer, inboxReducer} from 'redux/reducer';
import {persistReducer, persistStore} from 'redux-persist';
import logger, {createLogger} from 'redux-logger';

const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
};
const rootReducer = combineReducers({
  auth: authReducer,
  userInfo: userInfoReducer,
  getLists: getRequestReducer,
  inboxList: inboxReducer
});

const persistedReducer = persistReducer(persistConfig, rootReducer);

const store = createStore(persistedReducer, applyMiddleware(createLogger()));

let persistedStore = persistStore(store);

export {store, persistedStore};
