import Axios from 'axios';
import {GetCompanies, GetJobs, resumeRequest,seeAllResumeRequest, 
  viewProgress,checkResponse as callResponseApi, draftDislike, draftLike} from 'redux/apiConstants';
import {COMPAINES_LIST_STATE,JOB_LIST_STATE} from 'redux/constants'


const companyList  = (data) => {
  // console.log('DAA_--------------')
  return ({type:COMPAINES_LIST_STATE, payload:{listOfCompanies:data.companies}})
}

const jobList  = (data) => {
  // console.log('DAA_--------------')
  return ({type:JOB_LIST_STATE, payload:{listOfJobs:data.jobs}})
}
// ------------------ASYNC
export const GetCompaniesList = async (onError) => {
  try {
    const response = await Axios.get(GetCompanies);
      return response.data.success
      ? companyList(response.data)
        :
        ( 
          console.log('I am in first error'),
          onError(response.data.success)
        )
        return result
  } catch (e) {
    // console.log('CATCHING STATE');
    // console.log(e);
    onError(e);
  }
};


export const GetJobsList = async (onError) => {
  try {
    const response = await Axios.get(GetJobs);
    console.log("===========JOB RESULT===============")
    console.log(response)
      return response.data.success
      ? jobList(response.data)
        :
        ( 
          console.log('I am in first error'),
          onError(response.data.success)
        )
        return result
  } catch (e) {
    console.log('CATCHING STATE');
    console.log(e);
    onError(e);
  }
};


export const requestResume = async(userId,onError) => {
  try{
    const response = await Axios.get(`${resumeRequest}${userId}`)
    console.log('response result')
    return response.data.success ? response.data : onError(response.data)
  }
  catch(e){
    console.log('error fetching')
    console.log(e)
    onError(e)
  }
}

// ========= cv builder

export const getAllResumeReuqests = async(userId, onError) => {
  // console.log('userId')
  // console.log(userId)
  try{
    const response = await Axios.get(`${seeAllResumeRequest}${userId}`)
    return response.data.success ? response.data.requests : onError(response.data)
  }
  catch(e){
    console.log('error fetching')
    // console.log(e)
    onError(e)
  }
}

export const checkProgress = async(userId, onError) => {
  // console.log('userId')
  // console.log(userId)
  try{
    const response = await Axios.get(`${viewProgress}${userId}`)
    // console.log('response')
    // console.log(response)
    return response.data.success ? response.data : onError(response.data)
  }
  catch(e){
    console.log('error fetching')
    console.log(e)
    onError(e)
  }
}

export const draftLiked = async(draftDetails, onError)=>{
  console.log('draftDetails')
  console.log(draftDetails)

  try{
    const {req_id, draft_id} = draftDetails
    const response = await Axios.get(`${draftLike}${req_id}/draft/${draft_id}`)
    console.log('response api')
    console.log(response)
    return response.data.success ? response.data.feedback : onError(response.data)
  }
  catch(e){
    console.log('cating state')
    console.log(e)
    onError(e)
  }
}

export const draftDisliked = async(userDetails, onError)=>{
  // console.log('userDetails')
  // console.log(userDetails)
  const {id,req_id,comments} = userDetails
  // console.log(id)
  // console.log(req_id)
  // console.log(comments)

  try{
    const response = await Axios.post(draftDislike,{
      draft_id:id,
      req_id:req_id,
      comments:comments
    })
    console.log('dislike response api')
    console.log(response)
    // return response.data.success ? response.data : onError(response.data)
  }
  catch(e){
    console.log('cating state')
    console.log(e)
    onError(e)
  }
}

//  to check if feedback comments and like disklike
export const checkResponse = async(id, onError) => {
  try{
    const response = await Axios.get(`${callResponseApi}${id}`)
    console.log('response api')
    console.log(response.data.draft.feedback)
    return response.data.success ? response.data.draft : onError(response.data)

  }
  catch(e){
    console.log('error')
    onError(e)
  }

}