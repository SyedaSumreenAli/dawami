import {
  USER_LOGIN,
  REGISTER_USER,
  FORGET_TOKEN,
  ADD_USER_INFO,
  USER_LOGOUT,
  REMOVE_FORGET_TOKEN,
  REMOVE_USER_INFO,
} from '../constants';
import axios from 'axios';
import {
  LoginUser,
  Registration,
  ForgetPasswordSendEmail,
  ForgetPasswordVerifyCode,
  ResetPassword,
  Google,
  Facebook,
  CheckUserName,
  CheckEmail,
  RemoveEmail,
  UserVerfication,
} from '../apiConstants';
import {addUserInfo} from './user';
import {batch, useDispatch} from 'react-redux';
// import {ADD_USER_INFO} from 'redux/constants';

// export const addUserInfo = userInfo => {
//   return {type: ADD_USER_INFO, payload: userInfo};
// };
const login = userDetails => {
  return {type: USER_LOGIN, payload: userDetails};
};
const registerSuccess = value => {
  return {type: REGISTER_USER, payload: value};
};

const forgetPasswordToken = value => {
  return {type: FORGET_TOKEN, payload: {code: value}};
};

// ============== ASYNC ACTIONS=======================
export const loginUser = async (userData, onError) => {
  try {
     console.log('userid'+ userData)
    const response = await axios.post(LoginUser, {
      email: userData.email,
      password: userData.password,
    });
    return response.data.success
      ? login(response.data)
      : (console.log('i am in first state error '),
        console.log(response.data),
        onError(response.data));
  } catch (err) {
    console.log('i ma in cating state');
    console.log(err);
    onError(err);
  }
};
export const saveUser = userInfo => {
  return {type: ADD_USER_INFO, payload: userInfo};
};
//if not remember me
export const removeUserInfo = () => {
  return {type: REMOVE_USER_INFO, payload: {user: null}};
};

export const registerUser = async (userData, onError) => {
  const details = {
    name: userData.name,
    email: userData.email,
    password: userData.password,
    conf_password: userData.confirmPassword,
    username: userData.userName,
  };
  try {
    const response = await axios.post(Registration, details);
    // console.log("RESPONSE USER REG")
    // console.log(response)
    return response.data.success
      ? // ? registerSuccess(response.data)   in case to move just register user original authenticated user routes
        (console.log('SUCCEEDED IN REGISTRATION'),
        // console.log(response.data),
        response.data)
      : (console.log('NOT SUCCESSED IN REGISTRATION'),
        // console.log(response.data),
        onError(response.data));
  } catch (error) {
    onError(error);
  }
};

export const forgetPassword = async (email, onError) => {
  try {
    const response = await axios.post(ForgetPasswordSendEmail, {email: email});
    return response.data.success
      ? (console.log('FORGET DATA ACTION', response.data),
        forgetPasswordToken(response.data.user))
      : (console.log('Error'), onError(response.data));
  } catch (err) {
    console.log('CATCHING STATE');
    onError(err);
  }
};

export const verifyCode = async (userData, onError) => {
  try {
    const response = await axios.post(ForgetPasswordVerifyCode, {
      email: userData.email,
      token: userData.token,
    });
    return response.data;
  } catch (e) {
    console.log('CATCHING STATE');
    onError(e);
  }
};

export const resetPassword = async (userData, onError) => {
  try {
    const response = await axios.post(ResetPassword, {
      email: userData.email,
      password: userData.password,
    });
    return response.data;
  } catch (e) {
    console.log('CATCHING STATE');
    onError(e);
  }
};

export const logout = () => {
  return {type: USER_LOGOUT, payload: {success: false}};
};

export const goWithgoogle = async (user, onError) => {
  // console.log('user coming')
  // console.log(user)
  try {
    const response = await axios.post(Google, {
      email: user.email,
      name: user.name,
    });
    // console.log(response.data);
    return response.data.success 
      ? addUserInfo(response.data)
      : (console.log('FIRST ERROR STATE GOOGLE'), onError(response.message));
  } catch (err) {
    console.log('CATCHING STATE');
    console.log(err)
    onError(err);
  }
};

export const goWithFacebook = async (user, onError) => {
  try {
    const response = await axios.post(Facebook, {
      email: user.email,
      name: user.name,
    });
    console.log('response facebook actions');
    console.log(response);
    return response.data.success
      ? addUserInfo(response.data)
      : onError(response.message);
  } catch (err) {
    console.log('CATCHING STATE');
    console.log(err);
    onError(err);
  }
};

export const checkUserNameAvailability = async (username, onError) => {
  try {
    const response = await axios.get(`${CheckUserName}${username}`);
    console.log('FETCH USERNAME DETAIL');
    console.log(response);
    return response.data;
  } catch (e) {
    console.log('unable to search username in directory.. fectching error');
    onError(e);
  }
};

export const checkEmailAvailability = async (email, onError) => {
  console.log('EMAIL VALUE IN ACTION');
  console.log(email);
  try {
    const response = await axios.get(`${CheckEmail}${email}`);
    console.log('FETCH EMAIL DETAIL');
    console.log(response);
    return response.data;
  } catch (e) {
    console.log('unable to search EMAIL in directory.. fectching error');
    console.log(e);
    return onError(e);
  }
};

export const RemoveRegisteredAccount = async (email, onError) => {
  try{
    const response = await axios.get(`${RemoveEmail}${email}`)
    console.log(response)
    return response.data.success ? response.data : onError(response.data)
  }
  catch(e){
    console.log('CATCH REMOVEING ACCOUNT')
    console.log(e)
    onError(e)
  }
}

export const VerifyUser = async (userData, onError) => {
  try{
    const response = await axios.post(UserVerfication,{
      email:userData.email,
      token:userData.confirmation_code
    })
    return response.data.success ? 
      response.data
    : 
      onError(response.data)
  }
  catch(e){
    console.log('error')
    console.log(e)
    onError(e)
  }
}