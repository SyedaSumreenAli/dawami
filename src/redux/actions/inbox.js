export const FETCH_INBOX_PENDING = 'FETCH_INBOX_PENDING';
export const FETCH_INBOX_SUCCESS = 'FETCH_INBOX_SUCCESS';
export const FETCH_INBOX_ERROR = 'FETCH_INBOX_ERROR';

function fetchInboxPending() {
    return {
        type: FETCH_INBOX_PENDING
    }
}

function fetchInboxSuccess(inbox) {
    return {
        type: FETCH_INBOX_SUCCESS,
        inboxList: inboxList
    }
}

function fetchInboxsError(error) {
    return {
        type: FETCH_INBOX_ERROR,
        error: error
    }
}
