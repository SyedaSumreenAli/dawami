import {
  ADD_USER_INFO,
  SET_LANGUAGE,
  ADD_PROFILE_INFO,
  UPDATE_PROFILE_INFO,
  ADD_PREFERRED_JOB,
  ADD_EDUCATION
} from 'redux/constants';
import Axios from 'axios';
import {ProfileInfo, BasicInfoUpdate} from 'redux/apiConstants';

export const addUserInfo = userInfo => {
  return {type: ADD_USER_INFO, payload: userInfo};
};

export const addProfileInfo = ProfileInfo => {
  return {type: ADD_PROFILE_INFO, payload: ProfileInfo};
};

export const language = currentLanguage => {
  return {type: SET_LANGUAGE, payload: currentLanguage};
};

const updateInfo = updatedProfile => {
  return {type: UPDATE_PROFILE_INFO, payload: updatedProfile};
};
const addPreferJob = (preferred_data) => {
  return {type:ADD_PREFERRED_JOB, payload:preferred_data}
}
const addEducation = (educationData) => {
  return {type:ADD_EDUCATION, payload:educationData}
}
// Async Actions

export const GetProfileInfo = async (id, onError) => {
  try {
    const response = await Axios.get(`${ProfileInfo}${id}/profile`);
    console.log('user Info');
    console.log(response);
    return response.data.success
      ? addProfileInfo(response.data)
      : onError(response.data);
  } catch (e) {
    console.log('error fechting');
    onError(e);
  }
};

export const UpdateUserInfo = async (updatedDataDetails, onError) => {
  const user_id = updatedDataDetails.user_id;
  const updateDetails = {
    full_name: updatedDataDetails.full_name,
    date_of_birth: updatedDataDetails.date_of_birth,
    gender: updatedDataDetails.gender,
    nationality_id: updatedDataDetails.nationality_id,
    visa_status: updatedDataDetails.visa_status,
    residence_id: updatedDataDetails.residence_id,
    marital_status: updatedDataDetails.marital_status,
    no_of_dependents: updatedDataDetails.no_of_dependents,
    license_country: updatedDataDetails.license_country,
  };

  console.log(updatedDataDetails);
  try {
    const response = await Axios.post(
      `${BasicInfoUpdate}${user_id}/update`,
      updateDetails,
    );
    console.log(response);
    return response.data.success
      ? updateInfo(response.data.profile)
      : onError(response.data);
  } catch (e) {
    console.log('cating error');
    console.log(e);
    onError(e);
  }
};

// Phone Number
export const UpdateContactInfo = async (updateDetails, onError) => {
  const {id, phone} = updateDetails;
  // console.log(id)
  // console.log(phone)
  try {
    const response = await Axios.post(
      `${BasicInfoUpdate}${id}/update/contact`,
      {phone: phone},
    );
    // console.log(response)
    return response.data.success
      ? updateInfo(response.data.profile)
      : onError(response.data);
  } catch (e) {
    console.log('cation error');
    console.log(e);
    onError(e);
  }
};
// job title
export const AddJobTitle = async (userDetails, onError?) => {
  console.log('userDetails')
  console.log(userDetails)
  const updateDetail = {
    title: userDetails.title,
    country: userDetails.jobLocation,
    // country:"1",
    visa: userDetails.visaType,
    // visa:"1",
    category_id: userDetails.jobCategory,
    // category_id:'1',
    level: userDetails.jobLevel,
    // level: '1',
    industry_id: userDetails.jobIndustry,
    // industry_id: '1',
    type: userDetails.jobType,
    // type: '1',
    target_salary: userDetails.targetSalary,
    // target_salary:'222',
    summary: userDetails.summary,
    // summary: 'summary',
    target_currency: userDetails.targetCurrency,
    // target_currency: '1',
    notice_period: userDetails.noticePeriod,
    // notice_period:'1',
    userId: userDetails.id,
    // userId:'3',
  };
  // console.log('================BEFORE API REPSONSE=================')
  // console.log(updateDetail)
  // return true

  try {
    console.log('DATA BEFORE SUBMITTION========')
    console.log(updateDetail)
    const response = await Axios.post(
      `${BasicInfoUpdate}${updateDetail.userId}/update/preferred_job`,
      updateDetail,
    );
    console.log('API =========RESPONSE IS ================');
    console.log(response.data.perefer_job)
    return response.data.success? addPreferJob(response.data.perefer_job) : onError(response.data)
  } catch (e) {
    console.log('error catching');
    console.log(e);
    onError(e)
  }
};







// Education
export const AddEducation = async (userDetails, onError?) => {
  console.log('==========userDetails==========')
  console.log(userDetails)
  const updateDetail = {
    university_name :userDetails.uniName,
    degree : userDetails.degree,
    country_id: userDetails.country,
    major : userDetails.major,
    graduate_year : userDetails.year,
    graduate_month : userDetails.month,
    description: userDetails.summary
  };
  // console.log('================BEFORE API REPSONSE=================')
  // console.log(updateDetail)
  // return true

  try {
    console.log('DATA BEFORE SUBMITTION========')
    console.log(updateDetail)
    const response = await Axios.post(
      `${BasicInfoUpdate}${userDetails.id}/add/education`,updateDetail,
    );
    console.log('API =========RESPONSE IS ================');
    console.log(response.data.perefer_job)
    return response.data.success? addEducation(response.data.education) : onError(response.data)
  } catch (e) {
    console.log('error catching');
    console.log(e);
    onError(e)
  }
};
