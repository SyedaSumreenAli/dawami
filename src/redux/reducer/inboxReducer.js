import {FETCH_INBOX_PENDING, FETCH_INBOX_SUCCESS, FETCH_INBOX_ERROR} from '../actions/inbox';

const initialState = {
    pending: false,
    inbox: [],
    error: null
}

export function inboxReducer(state = initialState, action) {
    switch(action.type) {
        case FETCH_INBOX_PENDING: 
            return {
                ...state,
                pending: true
            }
        case FETCH_INBOX_SUCCESS:
            return {
                ...state,
                pending: false,
                inbox: action.payload
            }
        case FETCH_INBOX_ERROR:
            return {
                ...state,
                pending: false,
                error: action.error
            }
        default: 
            return state;
    }
}

export const getInbox = state => state.inbox;
export const getInboxPending = state => state.pending;
export const getInboxError = state => state.error;
