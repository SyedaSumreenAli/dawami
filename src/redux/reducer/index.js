import {authReducer} from './auth'
import {userInfoReducer} from './user'
import {getRequestReducer} from './getRequests'
import {inboxReducer} from './inboxReducer'

export {authReducer,userInfoReducer,getRequestReducer, inboxReducer}