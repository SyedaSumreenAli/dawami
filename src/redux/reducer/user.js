import {
  ADD_USER_INFO,
  REMOVE_USER_INFO,
  LANG_EN,
  SET_LANGUAGE,
  ADD_PROFILE_INFO,
  UPDATE_PROFILE_INFO,
  ADD_PREFERRED_JOB,
  ADD_EDUCATION,
} from 'redux/constants';
const initialState = {
  // currentLN : ''
  currentLN: LANG_EN,
  userData: {
    profile: {
      experiences: [],
      pereferred_job: {},
      educations: [],
      skills: [],
      hobbies: [],
      references: [],
      languages: [],
      certificates: [],
    },
    cities: [],
    countries: [],
  },
};

export const userInfoReducer = (state = initialState, action) => {
  switch (action.type) {
    case UPDATE_PROFILE_INFO:
      console.log('====REDUCER=========');
      console.log(action.payload);
      return {
        ...state,
        userData: {
          ...state.userData,
          profile: {
            ...state.userData.profile,
            ...action.payload,
          },
        },
      };
    // return {...state};
    case REMOVE_USER_INFO:
      return {...state, userInfo: action.payload};
    case SET_LANGUAGE:
      console.log(state.currentLN);
      return {...state, currentLN: action.payload};
    case ADD_PROFILE_INFO:
      return {...state.userInfo, userData: action.payload};
    case ADD_PREFERRED_JOB:
      return {
        ...state,
        userData: {
          ...state.userData,
          profile: {...state.userData.profile, pereferred_job: action.payload},
        },
      };
    case ADD_EDUCATION:
      return {
        ...state,
        userData: {
          ...state.userData,
          profile: {
            ...state.userData.profile,
            educations: [...state.userData.profile.educations, action.payload],
          },
        },
      };

    default:
      return {...state};
  }
};
