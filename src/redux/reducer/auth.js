import {
  USER_LOGIN,
  USER_LOGOUT,
  REGISTER_USER,
  FORGET_TOKEN,
  REMOVE_FORGET_TOKEN,
  ADD_USER_INFO,
} from '../constants';

const initialState = {
  status: false,
  reset_confirmation_code: null,
  requestedUserDetails:null
};

export const authReducer = (state = initialState, action) => {
  switch (action.type) {
    case USER_LOGIN:
      return {...state, status: action.payload.success};
      case ADD_USER_INFO:
      // console.log('its changing')
      // console.log(action.payload)
      return {...state, userBasicProfile: action.payload};
      case USER_LOGOUT:
        return {...state, status: action.payload.success};
    case REGISTER_USER:
      return {...state, status:action.payload.success};
    case FORGET_TOKEN:
      return {
        ...state,
        reset_confirmation_code: action.payload.code.confirmation_code,
        requestedUserDetails: action.payload.code,
      };
    case REMOVE_FORGET_TOKEN:
      console.log('REMOVE FORGET TOKEN IS CALLED');
      return {...state, reset_confirmation_code: null};
    case USER_LOGOUT:
      return {...state, status: false};
    default:
      return state;
  }
};

export default authReducer;
